package com.sadur.corp.restauratmanager.service;

import com.sadur.corp.restauratmanager.model.reservation.Bar;
import com.sadur.corp.restauratmanager.model.reservation.Lounge;
import com.sadur.corp.restauratmanager.model.reservation.ReservationSpot;
import com.sadur.corp.restauratmanager.model.reservation.Table;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

class ReservationSpotFactoryTest {

   @Test
    void shouldCreateBar(){
       //given
       int seatsNumber = 1;
       ReservationSpotFactory reservationSpotFactory = new ReservationSpotFactory();
       //when
       ReservationSpot result = reservationSpotFactory.create(seatsNumber);
       //then
       assertThat(result).isInstanceOf(Bar.class);
       assertThat(result.getSeatsNumber()).isEqualTo(1);
   }

    @Test
    void shouldCreateTable(){
        //given
        int seatsNumber = 4;
        ReservationSpotFactory reservationSpotFactory = new ReservationSpotFactory();
        //when
        ReservationSpot result = reservationSpotFactory.create(seatsNumber);
        //then
        assertThat(result).isInstanceOf(Table.class);
        assertThat(result.getSeatsNumber()).isEqualTo(4);
    }
    @Test
    void shouldCreateLounge(){
        //given
        int seatsNumber = 5;
        ReservationSpotFactory reservationSpotFactory = new ReservationSpotFactory();
        //when
        ReservationSpot result = reservationSpotFactory.create(seatsNumber);
        //then
        assertThat(result).isInstanceOf(Lounge.class);
        assertThat(result.getSeatsNumber()).isEqualTo(5);
    }

    @Test
    void shouldThrowIllegalStateException() {
        //given
        int seatsNumber = 0;
        ReservationSpotFactory reservationSpotFactory = new ReservationSpotFactory();
        //when
        Throwable result = catchThrowable(() -> reservationSpotFactory.create(seatsNumber));
        //then
        assertThat(result).isNotNull();
        assertThat(result).isInstanceOf(IllegalStateException.class);
        assertThat(result.getMessage()).contains(String.valueOf(seatsNumber));
    }

    @Test
    void shouldThrowTooManySeats() {
        //given
        int seatsNumber = 9;
        ReservationSpotFactory reservationSpotFactory = new ReservationSpotFactory();
        //when
        Throwable result = catchThrowable(() -> reservationSpotFactory.create(seatsNumber));
        //then
        assertThat(result).isNotNull();
        assertThat(result).isInstanceOf(IllegalStateException.class);
        assertThat(result.getMessage()).contains(String.valueOf(seatsNumber));
    }
}