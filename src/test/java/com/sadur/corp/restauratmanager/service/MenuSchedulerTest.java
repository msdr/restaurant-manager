package com.sadur.corp.restauratmanager.service;

import com.sadur.corp.restauratmanager.model.menu.Menu;
import com.sadur.corp.restauratmanager.repository.MenuRepository;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@ExtendWith(MockitoExtension.class)
class MenuSchedulerTest {

    @InjectMocks
    private MenuScheduler menuScheduler;
    @Mock
    private MenuRepository menuRepository;

    @ParameterizedTest(name = "[{index}] - {0}")
    @MethodSource("paramsForDateTest")
    void shouldFilterAvailableMenus(String name, LocalDate from, LocalDate till, int saveInvoke) {
        //given
        List<Menu> menus = createTestMenu(from, till);
        ArgumentCaptor<Menu> menuCaptor = ArgumentCaptor.forClass(Menu.class);

        when(menuRepository.findByAvailableTrue()).thenReturn(menus);
        //when
        menuScheduler.menuChecker();
        //then
        verify(menuRepository, times(saveInvoke)).save(menuCaptor.capture());
        if(saveInvoke == 1) {
            assertThat(menuCaptor.getValue().isAvailable()).isFalse();
        }
    }

    private static Stream<Arguments> paramsForDateTest(){
        return Stream.of(
                Arguments.of("from is null & till is null", null, null, 0),
                Arguments.of("from is null & till is before", null, LocalDate.now().minusDays(1L), 1),
                Arguments.of("from is before & till is null", LocalDate.now().minusDays(3L), null, 0),
                Arguments.of("from is before & till is before", LocalDate.now().minusDays(3L), LocalDate.now().minusDays(1L), 1),
                Arguments.of("from is before & till is after", LocalDate.now().minusDays(3L), LocalDate.now().plusDays(2L), 0),
                Arguments.of("from is after & till is after", LocalDate.now().plusDays(2L), LocalDate.now().plusDays(5L), 1),
                Arguments.of("from is now & till is after", LocalDate.now(), LocalDate.now().plusDays(5L), 0),
                Arguments.of("from is before & till is now", LocalDate.now().minusDays(2L), LocalDate.now(), 0),
                Arguments.of("from is now & till is now", LocalDate.now(), LocalDate.now(), 0),
                Arguments.of("from is null & till is after", null, LocalDate.now().plusDays(5L), 0),
                Arguments.of("from is after & till is null", LocalDate.now().plusDays(5L), null, 1),
                Arguments.of("from is null & till is now", null, LocalDate.now(), 0),
                Arguments.of("from is now & till is null", LocalDate.now(), null, 0)
        );

    }

    private List<Menu> createTestMenu(LocalDate from, LocalDate till) {
        Menu menu = new Menu();
        menu.setAvailableFrom(from);
        menu.setAvailableTill(till);
        menu.setAvailable(true);
        return List.of(menu);
    }

}