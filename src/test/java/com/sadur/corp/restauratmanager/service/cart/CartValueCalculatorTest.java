/*
package com.sadur.corp.restauratmanager.service.cart;

import com.sadur.corp.restauratmanager.model.cart.Cart;
import com.sadur.corp.restauratmanager.model.cart.Promotion;
import com.sadur.corp.restauratmanager.model.menu.Currency;
import com.sadur.corp.restauratmanager.model.menu.ItemType;
import com.sadur.corp.restauratmanager.model.menu.MenuItem;
import com.sadur.corp.restauratmanager.model.menu.PromotionBundle;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CartValueCalculatorTest {

    private CartValueCalculator cartValueCalculator = new CartValueCalculator();

    @Test
    void shouldCalculateHappyHour() {
        //given
        Cart cart = mock(Cart.class);
        List<MenuItem> menuItems = List.of(menuItem(BigDecimal.valueOf(50), 1L),
                menuItem(BigDecimal.valueOf(50), 1L),
                menuItem(BigDecimal.valueOf(50), 1L),
                menuItem(BigDecimal.valueOf(50), 1L));
        when(cart.getPromotion()).thenReturn(Promotion.HAPPY_HOUR);
        when(cart.getMenuItems()).thenReturn(menuItems);
        //when
        BigDecimal result = cartValueCalculator.calculate(cart);
        //then
        assertThat(result).isEqualTo(BigDecimal.valueOf(100));

    }

    @Test
    void shouldNotCalculateHappyHours() {
        //given
        Cart cart = mock(Cart.class);
        List<MenuItem> menuItems = List.of(menuItem(BigDecimal.valueOf(50), 1L),
                menuItem(BigDecimal.valueOf(50), 1L),
                menuItem(BigDecimal.valueOf(50), 1L));
        when(cart.getPromotion()).thenReturn(Promotion.HAPPY_HOUR);
        when(cart.getMenuItems()).thenReturn(menuItems);
        //when
        BigDecimal result = cartValueCalculator.calculate(cart);
        //then
        assertThat(result).isEqualTo(BigDecimal.valueOf(150));

    }

    @Test
    void shouldCalculateDiscount10() {
        //given
        Cart cart = mock(Cart.class);
        List<MenuItem> menuItems = List.of(menuItem(BigDecimal.valueOf(50), 1L),
                menuItem(BigDecimal.valueOf(50), 1L),
                menuItem(BigDecimal.valueOf(50), 1L),
                menuItem(BigDecimal.valueOf(50), 1L));
        when(cart.getPromotion()).thenReturn(Promotion.DISCOUNT_10);
        when(cart.getMenuItems()).thenReturn(menuItems);
        //when
        BigDecimal result = cartValueCalculator.calculate(cart);
        //then
        assertThat(result).isEqualTo(BigDecimal.valueOf(180));
    }
    @Test
    void shouldCalculateTwoForOneDiscount() {
        //given
        Cart cart = mock(Cart.class);
        List<MenuItem> menuItems = List.of(menuItem(BigDecimal.valueOf(50), 1L),
                menuItem(BigDecimal.valueOf(50), 1L));
        when(cart.getPromotion()).thenReturn(Promotion.TWO_FOR_ONE);
        when(cart.getMenuItems()).thenReturn(menuItems);
        //when
        BigDecimal result = cartValueCalculator.calculate(cart);
        //then
        assertThat(result).isEqualTo(BigDecimal.valueOf(50));
    }

    @Test
    void shouldCalculateTwoForOneDiscountWithThreeItems() {
        //given
        Cart cart = mock(Cart.class);
        List<MenuItem> menuItems = List.of(menuItem(BigDecimal.valueOf(50), 1L),
                menuItem(BigDecimal.valueOf(50), 1L),
                menuItem(BigDecimal.valueOf(50), 1L),
                menuItem(BigDecimal.valueOf(50), 1L),
                menuItem(BigDecimal.valueOf(100), 2L),
                menuItem(BigDecimal.valueOf(100), 2L),
                menuItem(BigDecimal.valueOf(100), 2L));
        when(cart.getPromotion()).thenReturn(Promotion.TWO_FOR_ONE);
        when(cart.getMenuItems()).thenReturn(menuItems);
        //when
        BigDecimal result = cartValueCalculator.calculate(cart);
        //then
        assertThat(result).isEqualTo(BigDecimal.valueOf(300));
    }

    @Test
    void shouldCalculateStandardSetDiscount() {
        //given
        Cart cart = mock(Cart.class);
        PromotionBundle standardBundle = new PromotionBundle(true, false, false);
        //PromotionBundle extraBundle = new PromotionBundle(true, true, false);
        //PromotionBundle premiumBundle = new PromotionBundle(true, true, true);
        MenuItem menuItem1 = createSetMenuItem(standardBundle, BigDecimal.valueOf(100), ItemType.MAIN);
        MenuItem menuItem2 = createSetMenuItem(standardBundle, BigDecimal.valueOf(100), ItemType.DESSERT);
        MenuItem menuItem3 = createSetMenuItem(standardBundle, BigDecimal.valueOf(100), ItemType.DRINK);
        List<MenuItem> menuItems = List.of(menuItem1, menuItem2, menuItem3);
        when(cart.getPromotion()).thenReturn(Promotion.DISCOUNT_SET);
        when(cart.getMenuItems()).thenReturn(menuItems);
        //when
        BigDecimal result = cartValueCalculator.calculate(cart);
        //then
        assertThat(result).isEqualTo(BigDecimal.valueOf(270));
    }

    @Test
    void shouldCalculateExtraSetDiscount() {
        //given
        Cart cart = mock(Cart.class);
        PromotionBundle extraBundle = new PromotionBundle(true, true, false);
        //PromotionBundle premiumBundle = new PromotionBundle(true, true, true);
        MenuItem menuItem1 = createSetMenuItem(extraBundle, BigDecimal.valueOf(100), ItemType.MAIN);
        MenuItem menuItem2 = createSetMenuItem(extraBundle, BigDecimal.valueOf(100), ItemType.DESSERT);
        MenuItem menuItem3 = createSetMenuItem(extraBundle, BigDecimal.valueOf(100), ItemType.DRINK);
        List<MenuItem> menuItems = List.of(menuItem1, menuItem2, menuItem3);
        when(cart.getPromotion()).thenReturn(Promotion.DISCOUNT_SET);
        when(cart.getMenuItems()).thenReturn(menuItems);
        //when
        BigDecimal result = cartValueCalculator.calculate(cart);
        //then
        assertThat(result).isEqualTo(BigDecimal.valueOf(270));
    }

    @Test
    void shouldCalculatePremiumSetDiscount() {
        //given
        Cart cart = mock(Cart.class);
        PromotionBundle premiumBundle = new PromotionBundle(true, true, true);
        MenuItem menuItem1 = createSetMenuItem(premiumBundle, BigDecimal.valueOf(100), ItemType.MAIN);
        MenuItem menuItem2 = createSetMenuItem(premiumBundle, BigDecimal.valueOf(100), ItemType.DESSERT);
        MenuItem menuItem3 = createSetMenuItem(premiumBundle, BigDecimal.valueOf(100), ItemType.DRINK);
        List<MenuItem> menuItems = List.of(menuItem1, menuItem2, menuItem3);
        when(cart.getPromotion()).thenReturn(Promotion.DISCOUNT_SET);
        when(cart.getMenuItems()).thenReturn(menuItems);
        //when
        BigDecimal result = cartValueCalculator.calculate(cart);
        //then
        assertThat(result).isEqualTo(BigDecimal.valueOf(270));
    }
    @Test
    void shouldCalculateNoSetDiscount() {
        //given
        Cart cart = mock(Cart.class);
        PromotionBundle premiumBundle = new PromotionBundle(true, true, true);
        PromotionBundle noBundle = new PromotionBundle(false, false, false);
        MenuItem menuItem1 = createSetMenuItem(premiumBundle, BigDecimal.valueOf(100), ItemType.MAIN);
        MenuItem menuItem2 = createSetMenuItem(premiumBundle, BigDecimal.valueOf(100), ItemType.DESSERT);
        MenuItem menuItem3 = createSetMenuItem(premiumBundle, BigDecimal.valueOf(100), ItemType.DRINK);
        MenuItem menuItem4 = createSetMenuItem(noBundle, BigDecimal.valueOf(100), ItemType.MAIN);
        List<MenuItem> menuItems = List.of(menuItem1, menuItem2, menuItem3, menuItem4);
        when(cart.getPromotion()).thenReturn(Promotion.DISCOUNT_SET);
        when(cart.getMenuItems()).thenReturn(menuItems);
        //when
        BigDecimal result = cartValueCalculator.calculate(cart);
        //then
        assertThat(result).isEqualTo(BigDecimal.valueOf(370));
    }
    @Test
    void shouldNotCalculateWrongBundle() {
        //given
        Cart cart = mock(Cart.class);
        PromotionBundle premiumBundle = new PromotionBundle(true, true, true);
        PromotionBundle noBundle = new PromotionBundle(false, false, false);
        MenuItem menuItem1 = createSetMenuItem(premiumBundle, BigDecimal.valueOf(100), ItemType.MAIN);
        MenuItem menuItem2 = createSetMenuItem(premiumBundle, BigDecimal.valueOf(100), ItemType.DESSERT);
        MenuItem menuItem3 = createSetMenuItem(premiumBundle, BigDecimal.valueOf(100), ItemType.DESSERT);
        MenuItem menuItem4 = createSetMenuItem(noBundle, BigDecimal.valueOf(100), ItemType.MAIN);
        List<MenuItem> menuItems = List.of(menuItem1, menuItem2, menuItem3, menuItem4);
        when(cart.getPromotion()).thenReturn(Promotion.DISCOUNT_SET);
        when(cart.getMenuItems()).thenReturn(menuItems);
        //when
        BigDecimal result = cartValueCalculator.calculate(cart);
        //then
        assertThat(result).isEqualTo(BigDecimal.valueOf(400));
    }

    @Test
    void shouldCalculateSimpleDiscountForEvenElements() {
        //given
        Cart cart = mock(Cart.class);
                List<MenuItem> menuItems = List.of(menuItem(BigDecimal.valueOf(100), 1L),
                menuItem(BigDecimal.valueOf(100), 1L),
                menuItem(BigDecimal.valueOf(100), 1L),
                menuItem(BigDecimal.valueOf(100), 1L));
        when(cart.getPromotion()).thenReturn(Promotion.DISCOUNT);
        when(cart.getMenuItems()).thenReturn(menuItems);
        //when
        BigDecimal result = cartValueCalculator.calculate(cart);
        //then
        assertThat(result).isEqualTo(BigDecimal.valueOf(350.00f));
    }
    @Test
    void shouldCalculateSimpleDiscountForOddElements() {
        //given
        Cart cart = mock(Cart.class);
        List<MenuItem> menuItems = List.of(menuItem(BigDecimal.valueOf(100), 1L),
                menuItem(BigDecimal.valueOf(100), 1L),
                menuItem(BigDecimal.valueOf(100), 1L),
                menuItem(BigDecimal.valueOf(100), 1L),
                menuItem(BigDecimal.valueOf(100), 1L),
                menuItem(BigDecimal.valueOf(100), 1L),
                menuItem(BigDecimal.valueOf(100), 1L));
        when(cart.getPromotion()).thenReturn(Promotion.DISCOUNT);
        when(cart.getMenuItems()).thenReturn(menuItems);
        //when
        BigDecimal result = cartValueCalculator.calculate(cart);
        //then
        assertThat(result).isEqualTo(BigDecimal.valueOf(65000l, 2));
    }

    @Test
    void shouldCalculateTotalPercentDiscountForEvenElements(){
        //given
        Cart cart = mock(Cart.class);
        List<MenuItem> menuItems = List.of(menuItem(BigDecimal.valueOf(100), 1L),
                menuItem(BigDecimal.valueOf(100), 1L),
                menuItem(BigDecimal.valueOf(100), 1L),
                menuItem(BigDecimal.valueOf(100), 1L));
        when(cart.getPromotion()).thenReturn(Promotion.TOTAL_CART_PERCENT_DISCOUNT);
        when(cart.getMenuItems()).thenReturn(menuItems);
        //when
        BigDecimal result = cartValueCalculator.calculate(cart);
        //then
        assertThat(result).isEqualTo(BigDecimal.valueOf(26800l, 2));

    }
    

    @Test
    void shouldCalculateTotalPercentDiscountForOddElements(){
        //given
        Cart cart = mock(Cart.class);
        List<MenuItem> menuItems = List.of(menuItem(BigDecimal.valueOf(9999, 2), 1L),
                menuItem(BigDecimal.valueOf(9999, 2), 1L),
                menuItem(BigDecimal.valueOf(9999, 2), 1L),
                menuItem(BigDecimal.valueOf(9999, 2), 1L),
                menuItem(BigDecimal.valueOf(9999, 2), 1L));
        when(cart.getPromotion()).thenReturn(Promotion.TOTAL_CART_PERCENT_DISCOUNT);
        when(cart.getMenuItems()).thenReturn(menuItems);
        //when
        BigDecimal result = cartValueCalculator.calculate(cart);
        //then
        assertThat(result).isEqualTo(BigDecimal.valueOf(33497, 2));
    }


    private MenuItem createSetMenuItem(PromotionBundle standardBundle, BigDecimal value, ItemType itemType) {
        return new MenuItem(value, itemType, standardBundle);
    }

    @Test
    void shouldCalculateDiscount20() {
        //given
        Cart cart = mock(Cart.class);
        List<MenuItem> menuItems = List.of(menuItem(BigDecimal.valueOf(50), 1L),
                menuItem(BigDecimal.valueOf(50), 1L),
                menuItem(BigDecimal.valueOf(50), 1L),
                menuItem(BigDecimal.valueOf(50), 1L));
        when(cart.getPromotion()).thenReturn(Promotion.DISCOUNT_20);
        when(cart.getMenuItems()).thenReturn(menuItems);
        //when
        BigDecimal result = cartValueCalculator.calculate(cart);
        //then
        assertThat(result).isEqualTo(BigDecimal.valueOf(160));
    }

    private MenuItem menuItem(BigDecimal value, Long id) {
        MenuItem menuItem = new MenuItem("name", value, Currency.PLN);
        menuItem.setId(id);
        return menuItem;
    }
}*/
