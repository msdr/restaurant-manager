package com.sadur.corp.restauratmanager.service;

import com.sadur.corp.restauratmanager.model.dto.MenuItemDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class MenuImporterTest {

    @Test
    void shouldImportMenu() {
        //given
        File menuFile = getFile("menu.json");
        MenuImporter menuImporter = new MenuImporter();
        //then
        List<MenuItemDto> result = menuImporter.readFromJsonFile(menuFile);
        //when
        assertThat(result).isNotNull();
        assertThat(result.size()).isEqualTo(4);
    }

    @ParameterizedTest(name = "[{index}] - {0}")
    @MethodSource("paramsForShouldNotImport")
    void shouldLoggExceptionInParamTest(String name, String fileName) {
        //given
        File menuFile = getFile(fileName);
        MenuImporter menuImporter = new MenuImporter();
        //then
        List<MenuItemDto> result = menuImporter.readFromJsonFile(menuFile);
        //when
        assertThat(result).isNotNull();
        assertThat(result.size()).isEqualTo(0);
    }


    private static Stream<Arguments> paramsForShouldNotImport(){
        return Stream.of(
                Arguments.of("menu is with wrong Json file", "menuWithWrongJsonFile.json"),
                Arguments.of("menu has extra no supported fields", "menuWithExtraField.json"),
                Arguments.of("menu has less fields than necessary", "menuWithoutEssentialField.json"),
                Arguments.of("menu has wrong type of data","menuWithWrongDataType.json"),
                Arguments.of("menu singular Json instead of a table has ", "menuWithSingularJson.json"),
                Arguments.of("menu has a wrong enum for currency","menuWithWrongEnum.json")
        );
    }

    @Test
    void shouldImportMenuFromCsvFile() throws IOException {
        //given
        MenuImporter menuImporter = new MenuImporter();
        File menuFile = getFile("menu.csv");
        //when
        List<MenuItemDto> result = menuImporter.readFromCsvFile(menuFile);
        //then
        assertThat(result).isNotNull();
        assertThat(result.size()).isEqualTo(6);
    }

    private File getFile(String fileName) {
        return new File(getClass().getClassLoader().getResource(fileName).getFile());
    }
}
