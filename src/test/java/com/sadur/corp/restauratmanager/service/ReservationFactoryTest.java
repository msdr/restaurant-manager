package com.sadur.corp.restauratmanager.service;


import com.sadur.corp.restauratmanager.model.Reservation;
import com.sadur.corp.restauratmanager.model.dto.ReservationDto;
import com.sadur.corp.restauratmanager.model.reservation.ReservationSpot;
import com.sadur.corp.restauratmanager.model.reservation.SpotType;
import com.sadur.corp.restauratmanager.repository.ReservationRepository;
import com.sadur.corp.restauratmanager.repository.ReservationSpotRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.catchThrowable;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReservationFactoryTest {
    @Mock
    private ReservationSpotFactory reservationSpotFactory;
    @Mock
    private ReservationSpotRepository reservationSpotRepository;
    @Mock
    private ReservationRepository reservationRepository;
    @InjectMocks
    private ReservationFactory reservationFactory;


    @Test
    void shouldCreateBarReservation() {
        //given
        ReservationSpot spot = mock(ReservationSpot.class);
        ReservationSpot spot2 = mock(ReservationSpot.class);
        ReservationSpot spot3 = mock(ReservationSpot.class);
        ReservationSpot spot4 = mock(ReservationSpot.class);
        when(reservationSpotRepository.findBySpotType(SpotType.BAR)).thenReturn(List.of(spot, spot2, spot3, spot4));
        when(reservationRepository.findByReservationDateAndReservationStartBetween(any(), any(), any()))
                .thenReturn(List.of());
        //when
        Reservation result = reservationFactory.create(createBarDto());
        //then
        assertThat(result).isNotNull();
    }

    @Test
    void shouldCreateBarReservationWithTwoSeatsAlreadyTaken() {
        //given
        ReservationSpot spot = mock(ReservationSpot.class);
        ReservationSpot spot2 = mock(ReservationSpot.class);
        ReservationSpot spot3 = mock(ReservationSpot.class);
        ReservationSpot spot4 = mock(ReservationSpot.class);
        ReservationSpot spot5 = mock(ReservationSpot.class);
        Reservation reservation = new Reservation(spot, LocalTime.parse("13:35"), 2, LocalDate.parse("2021-07-01"));
        Reservation reservation2 = new Reservation(spot2, LocalTime.parse("13:35"), 2, LocalDate.parse("2021-07-01"));
        when(reservationSpotRepository.findBySpotType(SpotType.BAR)).thenReturn(List.of(spot, spot2, spot3, spot4, spot5));
        when(reservationRepository.findByReservationDateAndReservationStartBetween(any(), any(), any()))
                .thenReturn(List.of(reservation, reservation2));
        //when
        Reservation result = reservationFactory.create(createBarDto());
        //then
        assertThat(result).isNotNull();
    }

    @Test
    void shouldNotCreateBarReservationTooManyPeople() {
        //given
        ReservationDto dto = createBarDto();
        dto.setGuestNumber(12);
        ReservationSpot spot = mock(ReservationSpot.class);
        ReservationSpot spot2 = mock(ReservationSpot.class);
        ReservationSpot spot3 = mock(ReservationSpot.class);
        ReservationSpot spot4 = mock(ReservationSpot.class);
        ReservationSpot spot5 = mock(ReservationSpot.class);
        ReservationSpot spot6 = mock(ReservationSpot.class);
        Reservation reservation = new Reservation(spot, LocalTime.parse("13:35"), 2, LocalDate.parse("2021-07-01"));
        Reservation reservation2 = new Reservation(spot2, LocalTime.parse("13:35"), 2, LocalDate.parse("2021-07-01"));
        when(reservationSpotRepository.findBySpotType(SpotType.BAR)).thenReturn(List.of(spot, spot2, spot3, spot4, spot5, spot6));
        when(reservationRepository.findByReservationDateAndReservationStartBetween(any(), any(), any()))
                .thenReturn(List.of(reservation, reservation2));
        //when
        Throwable result = catchThrowable(() -> reservationFactory.create(dto));
        //then
        assertThat(result).isNotNull();
        Assertions.assertThat(result).isInstanceOf(IllegalStateException.class);
    }

    @Test
    void shouldCreateLoungeReservation() {
        //given
        ReservationSpot spot = mock(ReservationSpot.class);
        when(reservationSpotRepository.findBySpotType(SpotType.LOUNGE)).thenReturn(List.of(spot));
        when(reservationRepository.findByReservationDateAndReservationStartBetween(any(), any(), any()))
                .thenReturn(List.of());
        //when
        Reservation result = reservationFactory.create(createDto());
        //then
        assertThat(result).isNotNull();
    }

    @Test
    void shouldNotCreateLoungeReservationNoFreeSpots() {
        //given
        ReservationSpot spot = mock(ReservationSpot.class);
        Reservation reservation = new Reservation(spot, LocalTime.parse("13:35"), 2, LocalDate.parse("2021-07-01"));
        when(reservationSpotRepository.findBySpotType(SpotType.LOUNGE)).thenReturn(List.of(spot));
        when(reservationRepository.findByReservationDateAndReservationStartBetween(any(), any(), any()))
                .thenReturn(List.of(reservation));
        //when
        Throwable result = catchThrowable(() -> reservationFactory.create(createDto()));
        //then
        assertThat(result).isNotNull();
        Assertions.assertThat(result).isInstanceOf(IllegalStateException.class);
    }

    @Test
    void shouldCreateReservationOneFreeSpot() {
        //given
        ReservationSpot spot1 = mock(ReservationSpot.class);
        ReservationSpot spot2 = mock(ReservationSpot.class);
        Reservation reservation = new Reservation(spot2, LocalTime.parse("13:35"), 2, LocalDate.parse("2021-07-01"));
        when(reservationSpotRepository.findBySpotType(SpotType.LOUNGE)).thenReturn(List.of(spot1, spot2));
        when(reservationRepository.findByReservationDateAndReservationStartBetween(any(), any(), any()))
                .thenReturn(List.of(reservation));
        //when
        Reservation result = reservationFactory.create(createDto());
        //then
        assertThat(result).isNotNull();
    }

    @Test
    void shouldNotCreateLoungeReservationToManyPeople() {
        //given
        ReservationDto dto = createDto();
        dto.setGuestNumber(12);
        ReservationSpot spot = mock(ReservationSpot.class);
        when(reservationSpotRepository.findBySpotType(SpotType.LOUNGE)).thenReturn(List.of(spot));
        when(spot.getSeatsNumber()).thenReturn(5);
        when(reservationRepository.findByReservationDateAndReservationStartBetween(any(), any(), any()))
                .thenReturn(List.of());
        //when
        Throwable result = catchThrowable(() -> reservationFactory.create(dto));
        //then
        assertThat(result).isNotNull();
        Assertions.assertThat(result).isInstanceOf(IllegalStateException.class);
    }

    //Lounge SpotType
    private ReservationDto createDto() {
        List<Long> idList = new ArrayList<>();
        idList.add(1L);
        ReservationDto reservationDto = new ReservationDto();
        reservationDto.setReservationDate("2021-07-01");
        reservationDto.setReservationLength(1);
        reservationDto.setReservationStart("13:35");
        reservationDto.setSpotType(SpotType.LOUNGE);
        return reservationDto;
    }

    private ReservationDto createBarDto() {
        List<Long> idList = new ArrayList<>();
        idList.add(2L);
        ReservationDto reservationDto = new ReservationDto();
        reservationDto.setReservationDate("2021-07-01");
        reservationDto.setReservationLength(1);
        reservationDto.setReservationStart("13:35");
        reservationDto.setSpotType(SpotType.BAR);
        return reservationDto;
    }

}
