package com.sadur.corp.restauratmanager.service;


import com.sadur.corp.restauratmanager.gui.dto.ReservationDetailedViewDto;
import com.sadur.corp.restauratmanager.gui.dto.ReservationViewDto;
import com.sadur.corp.restauratmanager.model.Reservation;
import com.sadur.corp.restauratmanager.model.dto.ReservationDto;
import com.sadur.corp.restauratmanager.model.exceptions.CantCreateReservationException;
import com.sadur.corp.restauratmanager.model.exceptions.CantGenerateDetailedViewException;
import com.sadur.corp.restauratmanager.model.exceptions.CantUpdateReservationException;
import com.sadur.corp.restauratmanager.model.menu.Currency;
import com.sadur.corp.restauratmanager.model.menu.MenuItem;
import com.sadur.corp.restauratmanager.model.menu.Order;
import com.sadur.corp.restauratmanager.model.reservation.Bar;
import com.sadur.corp.restauratmanager.model.reservation.ReservationSpot;
import com.sadur.corp.restauratmanager.model.reservation.SpotType;
import com.sadur.corp.restauratmanager.model.user.User;
import com.sadur.corp.restauratmanager.repository.ReservationRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ReservationServiceTest {

    @InjectMocks
    private ReservationService reservationService;
    @Mock
    private ReservationFactory reservationFactory;
    @Mock
    private ReservationRepository reservationRepository;
    @Spy
    private ReservationMapper reservationMapper;

    private static final String COMMENT = "komentarz testowy";

    @Test
    void shouldAddReservation(){
        //given
        ReservationDto reservationDto = new ReservationDto();
        Reservation reservationToSave = new Reservation();
        reservationToSave.addComment(COMMENT);
        reservationToSave.setReservationStart(LocalTime.NOON);
        when(reservationFactory.create(reservationDto)).thenReturn(reservationToSave);
        ArgumentCaptor<Reservation> argumentCaptor = ArgumentCaptor.forClass(Reservation.class);
        when(reservationRepository.save(reservationToSave)).thenReturn(reservationToSave);
        //when
        Reservation result = reservationService.newReservation(reservationDto);
        //then
        verify(reservationRepository).save(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue().getReservationStart()).isEqualTo(LocalTime.NOON);
        assertThat(argumentCaptor.getValue().getComment()).isEqualTo(COMMENT);
    }

    @Test
    void shouldThrowCantCreateReservationException(){
        //given
        ReservationDto reservationDto = null;
        when(reservationFactory.create(reservationDto)).thenCallRealMethod();
        //when
        Throwable result = catchThrowable(() -> reservationService.newReservation(reservationDto));
        //then
        verify(reservationFactory).create(reservationDto);
        assertThat(result).isInstanceOf(CantCreateReservationException.class);
    }

    @Test
    void shouldUpdateReservation(){
        //given
        Long id = 1L;
        ReservationDto reservationDto = new ReservationDto();
        Reservation reservationToUpdate = new Reservation();
        reservationToUpdate.addComment(COMMENT);
        reservationToUpdate.setReservationStart(LocalTime.NOON);
        when(reservationFactory.update(reservationDto, id)).thenReturn(reservationToUpdate);
        ArgumentCaptor<Reservation> argumentCaptor = ArgumentCaptor.forClass(Reservation.class);
        when(reservationRepository.save(reservationToUpdate)).thenReturn(reservationToUpdate);
        //when
        Reservation result = reservationService.updateReservation(reservationDto, id);
        //then
        verify(reservationRepository).save(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue().getComment()).isEqualTo(COMMENT);
        assertThat(argumentCaptor.getValue().getReservationStart()).isEqualTo(LocalTime.NOON);
    }

    @ParameterizedTest(name = "[{index}] - {0}")
    @MethodSource("paramsForShouldNotUpdate")
    void shouldNotUpdate(String name, ReservationDto reservationDto, Long id){
        //given
        when(reservationFactory.update(reservationDto, id)).thenCallRealMethod();
        //when
        Throwable result = catchThrowable(()-> reservationService.updateReservation(reservationDto, id));
        //then
        assertThat(result).isNotNull();
        assertThat(result).isInstanceOf(CantUpdateReservationException.class);

    }

    private static Stream<Arguments> paramsForShouldNotUpdate(){
        return Stream.of(
                Arguments.of("id is null", new ReservationDto(), null),
                Arguments.of("dto is null", null, 1L),
                Arguments.of("dto and id are null", null, null)
                );
    }

    @Test
    void shouldPrepareReservationForView(){
        //given
        List<Reservation> reservations = Arrays.asList(getTestReservation(1L),getTestReservation(2L));
        when(reservationRepository.findAll()).thenReturn(reservations);
        //when
        List<ReservationViewDto> result = reservationService.getAllReservationsForView();
        //then
        assertThat(result).isNotNull();
        assertThat(result).isNotEmpty();
        assertThat(result.size()).isEqualTo(2);
        result.forEach(dto -> assertReservationDto(dto, dto.getId()));

    }

    @Test
    void shouldReturnEmptyList(){
        //given
        List<Reservation> reservations = new ArrayList<>();
        when(reservationRepository.findAll()).thenReturn(reservations);
        //when
        List<ReservationViewDto> result = reservationService.getAllReservationsForView();
        //then
        assertThat(result).isNotNull();
        assertThat(result).isEmpty();
    }

    @Test
    void shouldGetDetailedView(){
        //given
        Long id = 1L;
        Reservation reservation = getTestReservation(id);
        when(reservationRepository.findById(id)).thenReturn(Optional.of(reservation));
        //when
        ReservationDetailedViewDto result = reservationService.getDetailedView(id);
        //then
        assertThat(result.getId()).isEqualTo(id);
        assertThat(result.getReservationStart()).isEqualTo(LocalTime.NOON);
        assertThat(result.getReservationLength()).isEqualTo(1);
        assertThat(result.getGuestNumber()).isEqualTo(1);
        assertThat(result.getReservationDate()).isEqualTo(LocalDate.of(2009, 1, 16));
        assertThat(result.getSpotId()).isEqualTo(id);
        assertThat(result.getSpotType()).isEqualTo(SpotType.BAR.name());
        assertThat(result.getMenuItems()).isNotEmpty();
        assertThat(result.getMenuItems().size()).isEqualTo(1);
        assertThat(result.getMenuItems().get(0).getCurrency()).isEqualTo(Currency.EUR.name());
        assertThat(result.getMenuItems().get(0).getId()).isEqualTo(1L);
        assertThat(result.getMenuItems().get(0).getName()).isEqualTo("itemName");
        assertThat(result.getMenuItems().get(0).getValue()).isEqualTo(BigDecimal.TEN);
        assertThat(result.getValue()).isEqualTo(BigDecimal.TEN);
        assertThat(result.getComment()).isEqualTo(COMMENT);
        assertThat(result.getName()).isEqualToIgnoringCase("userName");

    }

    @Test
    void shouldThrowCantGenerateDetailedViewException(){
        //given
        Long id = 1L;
        when(reservationRepository.findById(id)).thenReturn(Optional.empty());
        //when
        Throwable result = catchThrowable(() -> reservationService.getDetailedView(id));
        //then
        assertThat(result).isNotNull();
        assertThat(result).isInstanceOf(CantGenerateDetailedViewException.class);
    }

    @Test
    void shouldThrowIllegalStateExceptionNullId(){
        //given
        Long id = null;
        //when
        Throwable result = catchThrowable(() -> reservationService.getDetailedView(id));
        //then
        verify(reservationRepository, times(0)).findById(any());
        assertThat(result).isNotNull();
        assertThat(result).isInstanceOf(IllegalStateException.class);
    }


    private void assertReservationDto(ReservationViewDto dto, Long id){

        assertThat(dto.getId()).isEqualTo(id);
        assertThat(dto.getReservationStart()).isEqualTo(LocalTime.NOON);
        assertThat(dto.getReservationLength()).isEqualTo(1);
        assertThat(dto.getGuestNumber()).isEqualTo(1);
        assertThat(dto.getReservationDate()).isEqualTo(LocalDate.of(2009, 1, 16));
    }


    private Reservation getTestReservation(Long id){
        ReservationSpot reservationSpot = new Bar();
        reservationSpot.setId(id);
        Order order = mock(Order.class);
        MenuItem menuItem = mock(MenuItem.class);
        User user = new User();
        user.setName("UserName");
        Reservation reservation = mock(Reservation.class);
        when(reservation.getId()).thenReturn(id);
        when(reservation.getReservationStart()).thenReturn(LocalTime.NOON);
        when(reservation.getReservationLength()).thenReturn(1);
        when(reservation.getReservedSpot()).thenReturn(reservationSpot);
        when(reservation.getReservationDate()).thenReturn(LocalDate.of(2009, 1, 16));
        when(reservation.getOrder()).thenReturn(order);
        when(order.getItems()).thenReturn(Collections.singletonList(menuItem));
        when(menuItem.getId()).thenReturn(id);
        when(menuItem.getValue()).thenReturn(BigDecimal.TEN);
        when(menuItem.getName()).thenReturn("itemName");
        when(menuItem.getCurrency()).thenReturn(Currency.EUR);
        when(order.getValue()).thenReturn(BigDecimal.TEN);
        when(reservation.getComment()).thenReturn(COMMENT);
        when(reservation.getUser()).thenReturn(user);
        return  reservation;
    }

}