package com.sadur.corp.restauratmanager.service;

import com.sadur.corp.restauratmanager.model.dto.MenuDto;
import com.sadur.corp.restauratmanager.model.dto.MenuItemDto;
import com.sadur.corp.restauratmanager.model.menu.Currency;
import com.sadur.corp.restauratmanager.model.menu.Menu;
import com.sadur.corp.restauratmanager.model.menu.MenuItem;
import com.sadur.corp.restauratmanager.repository.MenuRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class MenuServiceTest {

    @InjectMocks
    private MenuService menuService;
    @Mock
    private MenuRepository menuRepository;
    @Mock
    private MenuImporter menuImporter;
    @Spy
    private MenuItemFactory menuItemFactory;


    @Test
    void shouldImportMenuWithCsvFile() throws IOException {
        //given
        List<MenuItemDto> importedMenuItems = createMenuItems();
        File mockFile = mock(File.class);
        Long menuId = 1L;
        Menu menu = new Menu();
        menu.setMenuItems(new ArrayList<>());

        when(mockFile.getName()).thenReturn("testFile.csv");
        when(menuImporter.readFromCsvFile(mockFile)).thenReturn(importedMenuItems);
        when(menuRepository.findById(menuId)).thenReturn(Optional.of(menu));
        when(menuRepository.save(menu)).thenReturn(menu);

        //when
        Menu result = menuService.importMenuFromFile(mockFile, menuId);
        //then
        verify(menuImporter, times(1)).readFromCsvFile(mockFile);
        assertThat(result).isNotNull();
        assertThat(result.getMenuItems()).isNotNull();
        assertThat(result.getMenuItems().size()).isEqualTo(3);
    }

    @Test
    void shouldUpdateMenuFromFile() throws IOException {
        //given
        List<MenuItemDto> importedMenuItems = createMenuItems();
        File mockFile = mock(File.class);
        Long menuId = 1L;
        Menu menu = new Menu();
        menu.setMenuItems(new ArrayList<>());
        menu.getMenuItems().add(new MenuItem("kaszanka", BigDecimal.valueOf(345), Currency.DOL));

        when(mockFile.getName()).thenReturn("testFile.csv");
        when(menuImporter.readFromCsvFile(mockFile)).thenReturn(importedMenuItems);
        when(menuRepository.findById(menuId)).thenReturn(Optional.of(menu));
        when(menuRepository.save(menu)).thenReturn(menu);

        //when
        Menu result = menuService.importMenuFromFile(mockFile, menuId);
        //then
        verify(menuImporter, times(1)).readFromCsvFile(mockFile);
        assertThat(result).isNotNull();
        assertThat(result.getMenuItems()).isNotNull();
        assertThat(result.getMenuItems().size()).isEqualTo(3);
        assertThat(result.getMenuItems().get(0).getValue()).isEqualTo(BigDecimal.ONE);
        assertThat(result.getMenuItems().get(0).getCurrency()).isEqualTo(Currency.PLN);
    }

    @Test
    void shouldUpdateAndAddItemsToMenu() throws IOException {
        //given
        List<MenuItemDto> importedMenuItems = createMenuItems();
        File mockFile = mock(File.class);
        Long menuId = 1L;
        Menu menu = new Menu();
        menu.setMenuItems(new ArrayList<>());
        menu.getMenuItems().add(new MenuItem("mielone", BigDecimal.valueOf(345), Currency.DOL));

        when(mockFile.getName()).thenReturn("testFile.csv");
        when(menuImporter.readFromCsvFile(mockFile)).thenReturn(importedMenuItems);
        when(menuRepository.findById(menuId)).thenReturn(Optional.of(menu));
        when(menuRepository.save(menu)).thenReturn(menu);

        //when
        Menu result = menuService.importMenuFromFile(mockFile, menuId);
        //then
        verify(menuImporter, times(1)).readFromCsvFile(mockFile);
        assertThat(result).isNotNull();
        assertThat(result.getMenuItems()).isNotNull();
        assertThat(result.getMenuItems().size()).isEqualTo(4);
        assertThat(result.getMenuItems().get(0).getName()).isEqualTo("mielone");
        assertThat(result.getMenuItems().get(1).getName()).isEqualTo("kaszanka");
        assertThat(result.getMenuItems().get(2).getName()).isEqualTo("bigosik");
        assertThat(result.getMenuItems().get(3).getName()).isEqualTo("makulom");

    }

    @Test
    void shouldImportMenuWithJsonFile() throws IOException {
        //given
        List<MenuItemDto> importedMenuItems = createMenuItems();
        File mockFile = mock(File.class);
        Long menuId = 1L;
        Menu menu = new Menu();
        menu.setMenuItems(new ArrayList<>());

        when(mockFile.getName()).thenReturn("testFile.json");
        when(menuImporter.readFromJsonFile(mockFile)).thenReturn(importedMenuItems);
        when(menuRepository.findById(menuId)).thenReturn(Optional.of(menu));
        when(menuRepository.save(menu)).thenReturn(menu);

        //when
        Menu result = menuService.importMenuFromFile(mockFile, menuId);
        //then
        verify(menuImporter, times(1)).readFromJsonFile(mockFile);
        assertThat(result).isNotNull();
        assertThat(result.getMenuItems()).isNotNull();
        assertThat(result.getMenuItems().size()).isEqualTo(3);
    }

    @ParameterizedTest(name = "[{index}] - {0}")
    @MethodSource("paramsForNulls")
    void shouldThrowExceptionForNulls(String name, File mockFile, Long menuId) {
        //given
        Menu menu = new Menu();
        menu.setMenuItems(new ArrayList<>());

        if (mockFile != null) {
            when(mockFile.getName()).thenReturn("testFile.exe");
        }

        //when
        Throwable result = catchThrowable(() -> menuService.importMenuFromFile(mockFile, menuId));
        //then
        assertThat(result).isNotNull();
        assertThat(result).isInstanceOf(IllegalStateException.class);
    }

    private static Stream<Arguments> paramsForNulls() {
        return Stream.of(
                Arguments.of("Menu Id is null", mock(File.class), null),
                Arguments.of("Menu is null", null, 1L)
        );
    }

    @Test
    void shouldThrowWrongTypeOfMenuFileException() {

        //given
        File mockFile = mock(File.class);
        Long menuId = 1L;
        Menu menu = new Menu();
        menu.setMenuItems(new ArrayList<>());

        when(mockFile.getName()).thenReturn("testFile.exe");

        //when
        Throwable result = catchThrowable(() -> menuService.importMenuFromFile(mockFile, menuId));
        //then
        assertThat(result).isNotNull();
        assertThat(result).isInstanceOf(IllegalStateException.class);
    }

    private List<MenuItemDto> createMenuItems() {
        MenuItemDto dto1 = new MenuItemDto("kaszanka", BigDecimal.ONE, "PLN");
        MenuItemDto dto2 = new MenuItemDto("bigosik", BigDecimal.TEN, "PLN");
        MenuItemDto dto3 = new MenuItemDto("makulom", BigDecimal.ZERO, "PLN");

        return Arrays.asList(dto1, dto2, dto3);
    }

    @Test
    void shouldUpdateMenu() {
        //given
        MenuDto menuDto = new MenuDto(1L, "newMenu"
                , "2020-12-03", "2021-12-03", true);
        Menu menu = new Menu();
        menu.setId(1L);
        menu.setAvailableFrom(LocalDate.of(2010, 12,3));
        menu.setAvailableTill(LocalDate.of(2012, 4, 6));
        menu.setAvailable(false);
        menu.setMenuItems(List.of(new MenuItem()));

        when(menuRepository.findById(1L)).thenReturn(Optional.of(menu));
        when(menuRepository.save(menu)).thenReturn(menu);

        ArgumentCaptor<Menu> argumentCaptor = ArgumentCaptor.forClass(Menu.class);
        //when
        menuService.updateMenu(menuDto);
        //then
        verify(menuRepository).save(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue().getName()).isEqualTo(menuDto.getName());
        assertThat(argumentCaptor.getValue().getAvailableFrom()).isEqualTo(menuDto.getAvailableFrom());
        assertThat(argumentCaptor.getValue().getAvailableTill()).isEqualTo(menuDto.getAvailableTill());
        assertThat(argumentCaptor.getValue().isAvailable()).isEqualTo(menuDto.isAvailable());
        assertThat(argumentCaptor.getValue().getMenuItems().size()).isEqualTo(1);
    }

    @Test
    void shouldThrowExceptionWhileUpdatingMenu(){
        //given
        MenuDto menuDto = mock(MenuDto.class);
        when(menuRepository.findById(any())).thenReturn(Optional.empty());
        when(menuRepository.save(any())).thenReturn(mock(Menu.class));
        //when
        Throwable result = catchThrowable(() -> menuService.updateMenu(menuDto));
        //then
        verifyNoInteractions(menuRepository.save(any()));
        assertThat(result).isNotNull();
        assertThat(result).isInstanceOf(IllegalStateException.class);

    }

    @Test
    void shouldChangeMenuActiveStatus(){
        //given
        Menu menu = new Menu();
        menu.setActive(true);
        when(menuRepository.findById(any())).thenReturn(Optional.of(menu));
        when(menuRepository.save(any())).thenReturn(menu);
        ArgumentCaptor<Menu> argumentCaptor = ArgumentCaptor.forClass(Menu.class);
        //when
        menuService.changeMenuActiveStatus(1L);
        //then
        verify(menuRepository).save(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue().isActive()).isEqualTo(false);
    }
    @Test
    void shouldNotChangeMenuActiveStatus(){
        //given
        when(menuRepository.findById(any())).thenReturn(Optional.empty());
        when(menuRepository.save(any())).thenReturn(mock(Menu.class));
        //when
        menuService.changeMenuActiveStatus(1L);
        //then
        verifyNoInteractions(menuRepository.save(any()));
    }

}