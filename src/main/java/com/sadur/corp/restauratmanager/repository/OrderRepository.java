package com.sadur.corp.restauratmanager.repository;

import com.sadur.corp.restauratmanager.model.menu.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {


}
