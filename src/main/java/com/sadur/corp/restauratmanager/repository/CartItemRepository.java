package com.sadur.corp.restauratmanager.repository;

import com.sadur.corp.restauratmanager.model.cart.CartItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface CartItemRepository extends JpaRepository<CartItem, Long> {

        List<CartItem> findByCartIdIsNull();


}
    
