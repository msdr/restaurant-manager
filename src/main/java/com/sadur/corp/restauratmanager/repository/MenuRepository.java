package com.sadur.corp.restauratmanager.repository;

import com.sadur.corp.restauratmanager.model.menu.Menu;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface MenuRepository extends JpaRepository<Menu, Long> {

    List<Menu> findByAvailableTrue();
    List<Menu> findByActiveTrue();
}
