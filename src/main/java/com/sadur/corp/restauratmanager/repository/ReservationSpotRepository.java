package com.sadur.corp.restauratmanager.repository;

import com.sadur.corp.restauratmanager.model.reservation.ReservationSpot;
import com.sadur.corp.restauratmanager.model.reservation.SpotType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReservationSpotRepository extends JpaRepository<ReservationSpot,Long> {

    List<ReservationSpot> findBySpotType(SpotType spotType);
}
