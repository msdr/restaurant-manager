package com.sadur.corp.restauratmanager.repository;

import com.sadur.corp.restauratmanager.model.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long> {

//    Reservation findByUser_Name(String name);

    @Query("SELECT r FROM Reservation r WHERE r.user.name = ?1")
    Reservation selectByUserName(String name);

    List<Reservation> findByReservationDateAndReservationStartBetween(LocalDate reservationDate,
                                                                      LocalTime start, LocalTime end);
}
