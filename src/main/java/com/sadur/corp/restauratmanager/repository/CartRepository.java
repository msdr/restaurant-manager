package com.sadur.corp.restauratmanager.repository;

import com.sadur.corp.restauratmanager.model.cart.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface CartRepository extends JpaRepository<Cart, Long> {

    @Query("FROM Cart WHERE user.id = :userId AND status = 'ACTIVE'")
    Optional<Cart> findCartForUser(@Param("userId") Long userId);
}
