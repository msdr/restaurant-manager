package com.sadur.corp.restauratmanager.repository;

import com.sadur.corp.restauratmanager.model.menu.MenuItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MenuItemRepository extends JpaRepository<MenuItem, Long> {

//@Query("select menuItem from menuItems where orderId = :orderId")
//List<MenuItem> getMenuItemsByOrder(@Param("orderId") Long orderId);
}
