package com.sadur.corp.restauratmanager.service.cart;

import com.sadur.corp.restauratmanager.gui.dto.CartDto;
import com.sadur.corp.restauratmanager.model.cart.Cart;
import com.sadur.corp.restauratmanager.model.cart.CartItem;
import com.sadur.corp.restauratmanager.model.user.User;
import com.sadur.corp.restauratmanager.repository.CartItemRepository;
import com.sadur.corp.restauratmanager.repository.CartRepository;
import com.sadur.corp.restauratmanager.repository.MenuItemRepository;
import com.sadur.corp.restauratmanager.repository.UserRepository;
import com.sadur.corp.restauratmanager.service.MenuItemMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.function.Consumer;

@Service
public class CartService {
    //wyciągniej wszystko z db
    //CRUD create, read, update, delete

    @Autowired
    private CartRepository cartRepository;
    @Autowired
    private CartValueCalculator cartValueCalculator;
    @Autowired
    private MenuItemMapper menuItemMapper;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MenuItemRepository menuItemRepository;
    @Autowired
    private CartMapper cartMapper;
    @Autowired
    private CartItemRepository cartItemRepository;

    public CartDto getDto(Authentication authentication){
        Cart cart = checkForCart(authentication);
        cartValueCalculator.calculateCart(cart);
        return cartMapper.mapFromCart(cart);
    }

    public Cart addToCart(Authentication authentication, Long menuItemId) {
        Cart cart = checkForCart(authentication);
        menuItemRepository.findById(menuItemId)
            .map(item -> new CartItem(item, item.getValue()))
            .ifPresent(cartItem -> processCartItem(cart, cartItem, cart::addToCart));
        return cartRepository.save(cart);
    }

    public Cart removeFromCart(Authentication authentication, Long menuItemId) {
        Cart cart = checkForCart(authentication);
        cartItemRepository.findById(menuItemId)
            .ifPresent(cartItem -> processCartItem(cart, cartItem, cart::removeFromCart));
        return cartRepository.save(cart);
    }

    private void processCartItem(final Cart cart, final CartItem cartItem, final Consumer<CartItem> process) {
        cartItem.setCart(cart);
        process.accept(cartItem);
    }

    public void showCart(Authentication authentication){
        Cart cart = checkForCart(authentication);

    }

    public void confirmCart(Authentication authentication) {
        userRepository.findByEmail(authentication.getName())
                .map(user -> cartRepository.findCartForUser(user.getId()))
                .orElseThrow(() -> new IllegalStateException("Can't find user"))
                .ifPresent(cart -> confirmCart(cart));

    }

    private void confirmCart(Cart cart) {
        cart.confirmCart();
        cartRepository.save(cart);
    }

    public Cart checkForCart(Authentication authentication) {
        User user = userRepository.findByEmail(authentication.getName())
                .orElseThrow(() -> new IllegalStateException("Can't find user"));

        return  cartRepository.findCartForUser(user.getId())
                .orElse(new Cart(user));
    }

    //view cart
    //add to cart - update cart
    //delete remove from the cart
    //


}
