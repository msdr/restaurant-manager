package com.sadur.corp.restauratmanager.service.cart;

import com.sadur.corp.restauratmanager.model.cart.Cart;
import com.sadur.corp.restauratmanager.model.cart.Promotion;
import com.sadur.corp.restauratmanager.model.menu.ItemType;
import com.sadur.corp.restauratmanager.model.menu.MenuItem;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class CartValueCalculator {

    public void calculateCart(Cart cart){
        cart.setCartValue(calculate(cart));
    }


    public BigDecimal calculate(Cart cart) {
        BigDecimal cartValue = calculateCartValue(cart);

        if (shouldNotCalculatePromotion(cart)) {
            return cartValue;
        }
         switch (cart.getPromotion()) {
             case HAPPY_HOUR:
             case DISCOUNT_10:
             case DISCOUNT_20:
                 return calculatePercentDiscount(cartValue, cart.getPromotion());
             case TWO_FOR_ONE:
                 return calculateTwoForOneDiscount(cart);
             //case DISCOUNT_SET:
              //   return calculateSetDiscount(cart, cartValue);
             case DISCOUNT:
                 return calculateSimpleDiscount(cart, cartValue);
             case TOTAL_CART_PERCENT_DISCOUNT: 
                 return calculateTotalPercentDiscount(cart, cartValue);
        };
        return BigDecimal.ZERO;
    }

    private BigDecimal calculateCartValue(Cart cart) {
        BigDecimal cartValue = cart.getMenuItems().stream().map(menuItem -> menuItem.getPrice())
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        return cartValue;
    }
    //FIXME Obtestowane na wartościach z groszami, do poprawy jest jeszcze metoda na "zwykłą procentową" zniżkę,
    //FIXME ale uważam, że się jej pozbędziemy skoro mamy bardziej wyrafinowany mechanizm niż odjęcie wartości od koszyka
    private BigDecimal calculateTotalPercentDiscount(Cart cart, BigDecimal cartValue) {
        int menuItemsCount = cart.getMenuItems().size();
        BigDecimal cartValueAfterDiscount = calculatePercentDiscount(cartValue, cart.getPromotion());
        BigDecimal totalDiscount = cartValue.subtract(cartValueAfterDiscount);
        BigDecimal unitItemDiscount = totalDiscount.divide(BigDecimal.valueOf(menuItemsCount), new MathContext(4, RoundingMode.HALF_UP));
        cart.getMenuItems().forEach(menuItem -> menuItem.setPrice(menuItem.getPrice().subtract(unitItemDiscount)));

        BigDecimal newCartValue = calculateCartValue(cart);
        BigDecimal discountDifference = cartValueAfterDiscount.subtract(newCartValue).abs();
        cart.getMenuItems().get(0).setPrice(cart.getMenuItems().get(0).getPrice().add(discountDifference));
        return calculateCartValue(cart);
    }

    private BigDecimal calculateSimpleDiscount(Cart cart, BigDecimal cartValue) {
        int menuItemsCount = cart.getMenuItems().size();
        BigDecimal unitItemDiscount = BigDecimal.valueOf(cart.getPromotion().getDiscountValue())
                .divide(BigDecimal.valueOf(menuItemsCount), new MathContext(3, RoundingMode.HALF_UP));
        cart.getMenuItems().forEach(menuItem -> menuItem.setPrice(menuItem.getPrice().subtract(unitItemDiscount)));
        BigDecimal newCartValue = calculateCartValue(cart);


        BigDecimal totalDiscount = BigDecimal.valueOf(cart.getPromotion().getDiscountValue());
        BigDecimal discount = cartValue.subtract(newCartValue).subtract(totalDiscount).abs();
        cart.getMenuItems().get(0).setPrice(cart.getMenuItems().get(0).getPrice().subtract(discount));
        return calculateCartValue(cart);
    }

    /*private BigDecimal calculateSetDiscount(Cart cart, BigDecimal cartValue) {
        Map<Boolean, List<MenuItem>> standardBundleList = cart.getMenuItems()
                .stream()
                .collect(Collectors.partitioningBy(menuItem -> menuItem.getPromotionBundle().isStandard()));

        if (standardBundleList.get(true).size() > 2 && isBundle(standardBundleList.get(true))) {
            BigDecimal bundleValue = calculateValue(standardBundleList, true);
            BigDecimal noBundleValue = calculateValue(standardBundleList, false);
            return calculatePercentDiscount(bundleValue, cart.getPromotion()).add(noBundleValue);
        }
        return cartValue;
    }*/

    private BigDecimal calculateValue(Map<Boolean, List<MenuItem>> standardBundleList, boolean isBundle) {
        return standardBundleList.get(isBundle).stream()
                .map(menuItem -> menuItem.getValue())
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private boolean isBundle(List<MenuItem> standardBundleList) {
        return containsItemType(standardBundleList, ItemType.MAIN) &&
                containsItemType(standardBundleList, ItemType.DESSERT) &&
                containsItemType(standardBundleList, ItemType.DRINK);
    }

    private boolean containsItemType(List<MenuItem> standardBundleList, ItemType main) {
        return standardBundleList.stream().map(menuItem -> menuItem.getItemType())
                .anyMatch(itemType -> itemType.equals(main));
    }

    private BigDecimal calculateTwoForOneDiscount(Cart cart) {
        MultiValuedMap<Long, BigDecimal> cartMap = new ArrayListValuedHashMap<>();
        cart.getMenuItems().forEach(menuItem -> cartMap.put(menuItem.getId(), menuItem.getPrice()));
        Map<Long, Collection<BigDecimal>> itemOnPromotion = cartMap.asMap().entrySet().stream()
                .filter(entry -> entry.getValue().size() > 1)
                .collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue()));

        itemOnPromotion.entrySet().forEach(entry -> entry.setValue(clearEvenValues(entry.getValue())));

        BigDecimal promotionValue = itemOnPromotion.values().stream().flatMap(value -> value.stream()).reduce(BigDecimal.ZERO, BigDecimal::add);

        return promotionValue;
    }

    private Collection<BigDecimal> clearEvenValues(Collection<BigDecimal> bigDecimalList) {
        List<BigDecimal> valueList = new ArrayList<>(bigDecimalList);
        List<BigDecimal> withoutEvenList = new ArrayList<>();
        for (int i = 0; i < bigDecimalList.size(); i++) {

            if (i % 2 == 0) {
                withoutEvenList.add(valueList.get(i));
            }
        }
        return withoutEvenList;
    }
    //FIXME zaokrąglanie nadaje się tylko do liczenia łatwych cen
    private BigDecimal calculatePercentDiscount(BigDecimal cartValue, Promotion promotion) {
        BigDecimal promotionValue = BigDecimal.valueOf(promotion.getDiscountPercent())
                .multiply(cartValue, new MathContext(5, RoundingMode.HALF_UP));
        return cartValue.subtract(promotionValue);
    }

    private boolean shouldNotCalculatePromotion(Cart cart) {
        return cart.getPromotion() == null
                || cart.getMenuItems().size() < cart.getPromotion().getMinimalItemsNumber();
    }

}
