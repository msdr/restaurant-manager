package com.sadur.corp.restauratmanager.service;

import com.sadur.corp.restauratmanager.gui.dto.MenuItemViewDto;
import com.sadur.corp.restauratmanager.gui.dto.ReservationDetailedViewDto;
import com.sadur.corp.restauratmanager.gui.dto.ReservationViewDto;
import com.sadur.corp.restauratmanager.model.Reservation;
import com.sadur.corp.restauratmanager.model.menu.MenuItem;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ReservationMapper {

    public ReservationViewDto mapToDto(Reservation reservation) {
        return new ReservationViewDto(reservation.getId(),
                reservation.getReservationStart(),
                reservation.getReservationLength(),
                reservation.getReservedSpot().getSeatsNumber(),
                reservation.getReservationDate());
    }

    public ReservationDetailedViewDto mapToDetailedDto(Reservation reservation){
        return new ReservationDetailedViewDto(
                reservation.getId(),
                reservation.getReservationStart(),
                reservation.getReservationLength(),
                reservation.getReservedSpot().getSeatsNumber(),
                reservation.getReservationDate(),
                reservation.getReservedSpot().getTableIndex(),
                reservation.getReservedSpot().getSpotType().name(),
                mapToMenuItemsDto(reservation.getOrder().getItems()),
                reservation.getOrder().getValue(),
                reservation.getComment(),
                reservation.getUser().getName());
    }


    private List<MenuItemViewDto> mapToMenuItemsDto(List<MenuItem> menuItems) {
       return menuItems.stream()
                .map(menuItem -> new MenuItemViewDto(menuItem))
                .collect(Collectors.toList());
    }
}
