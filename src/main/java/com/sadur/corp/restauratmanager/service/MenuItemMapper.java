package com.sadur.corp.restauratmanager.service;


import com.sadur.corp.restauratmanager.gui.dto.MenuItemDto;
import com.sadur.corp.restauratmanager.gui.dto.MenuItemEditDto;
import com.sadur.corp.restauratmanager.model.cart.CartItem;
import com.sadur.corp.restauratmanager.model.menu.MenuItem;
import org.springframework.stereotype.Component;
//FIXME promocja i itemtype do dodania
@Component
public class MenuItemMapper {

    public MenuItemDto mapFrom(MenuItem menuItem){
        MenuItemDto menuItemDto = new MenuItemDto();
        menuItemDto.setId(menuItem.getId());
        menuItemDto.setName(menuItem.getName());
        menuItemDto.setPrice(menuItem.getValue() + " " + menuItem.getCurrency().name());
        menuItemDto.setAssignedToMenu(!menuItem.getMenus().isEmpty());
        menuItemDto.setItemType(menuItem.getItemType());
        menuItemDto.setPromotionBundle(menuItem.getPromotionBundle());
        return  menuItemDto;
    }
    
    public MenuItemDto mapFrom(CartItem cartItem){
        MenuItemDto menuItemDto = new MenuItemDto();
        menuItemDto.setId(cartItem.getId());
        menuItemDto.setName(cartItem.getMenuItem().getName());
        menuItemDto.setPrice(cartItem.getPrice() + " " + cartItem.getMenuItem().getCurrency().name());
        menuItemDto.setAssignedToMenu(!cartItem.getMenuItem().getMenus().isEmpty());
        menuItemDto.setItemType(cartItem.getMenuItem().getItemType());
        menuItemDto.setPromotionBundle(cartItem.getMenuItem().getPromotionBundle());
        return  menuItemDto;
    }

    public MenuItemEditDto mapToEditDto(MenuItem menuItem){
        MenuItemEditDto dto = new MenuItemEditDto();
        dto.setId(menuItem.getId());
        dto.setName(menuItem.getName());
        dto.setValue(menuItem.getValue());
        dto.setCurrency(menuItem.getCurrency().name());
        dto.setAssignedMenuId(getMenuIds(menuItem));
        dto.setItemType(menuItem.getItemType().name());
        dto.setStandard(menuItem.getPromotionBundle().isStandard());
        dto.setExtra(menuItem.getPromotionBundle().isExtra());
        dto.setPremium(menuItem.getPromotionBundle().isPremium());
        return dto;
    }

    private Long getMenuIds(MenuItem menuItem) {
        return menuItem.getMenus()
                .stream()
                .map(menu -> menu.getId())
                .findFirst().orElse(null);
    }
}
