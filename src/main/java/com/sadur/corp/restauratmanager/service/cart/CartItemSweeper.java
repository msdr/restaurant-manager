package com.sadur.corp.restauratmanager.service.cart;


import com.sadur.corp.restauratmanager.model.cart.CartItem;
import com.sadur.corp.restauratmanager.repository.CartItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CartItemSweeper {

    @Autowired
    private CartItemRepository cartItemRepository;

    @Scheduled(cron ="${cart.sweeper.frequency}")
    public void cartItemSweeper() {
       List<CartItem> itemList = cartItemRepository.findByCartIdIsNull();
       cartItemRepository.deleteInBatch(itemList);
    }

}
