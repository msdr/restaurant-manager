package com.sadur.corp.restauratmanager.service;

import com.sadur.corp.restauratmanager.gui.dto.ReservationDetailedViewDto;
import com.sadur.corp.restauratmanager.gui.dto.ReservationViewDto;
import com.sadur.corp.restauratmanager.model.Reservation;
import com.sadur.corp.restauratmanager.model.dto.ReservationDto;
import com.sadur.corp.restauratmanager.model.exceptions.CantGenerateDetailedViewException;
import com.sadur.corp.restauratmanager.repository.ReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReservationService {

    @Autowired
    private ReservationFactory reservationFactory;
    @Autowired
    private ReservationRepository reservationRepository;
    @Autowired
    private ReservationMapper reservationMapper;

    public Reservation newReservation(ReservationDto reservationDto) {
        Reservation reservation = reservationFactory.create(reservationDto);
        return reservationRepository.save(reservation);
    }

    public List<Reservation> getAllReservations() {
        return reservationRepository.findAll();
    }

    public Reservation updateReservation(ReservationDto reservationDto, Long id) {
        Reservation reservationToUpdate = reservationFactory.update(reservationDto, id);
        return reservationRepository.save(reservationToUpdate);
    }

    public List<ReservationViewDto> getAllReservationsForView() {
        return getAllReservations().stream()
                .map(reservation -> reservationMapper.mapToDto(reservation))
                .collect(Collectors.toList());
    }

    public ReservationDetailedViewDto getDetailedView(Long id) {
        if (id == null) {
            throw new IllegalStateException("id is null, not cool");
        }

        return reservationRepository.findById(id)
                .map(reservation -> reservationMapper.mapToDetailedDto(reservation))
                .orElseThrow(() -> new CantGenerateDetailedViewException("Cant generate reservation view. Not found"));
    }
}