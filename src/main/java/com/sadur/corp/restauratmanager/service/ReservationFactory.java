package com.sadur.corp.restauratmanager.service;

import com.sadur.corp.restauratmanager.model.Reservation;
import com.sadur.corp.restauratmanager.model.dto.ReservationDto;
import com.sadur.corp.restauratmanager.model.exceptions.CantCreateReservationException;
import com.sadur.corp.restauratmanager.model.exceptions.CantUpdateReservationException;
import com.sadur.corp.restauratmanager.model.reservation.ReservationSpot;
import com.sadur.corp.restauratmanager.model.reservation.SpotType;
import com.sadur.corp.restauratmanager.repository.ReservationRepository;
import com.sadur.corp.restauratmanager.repository.ReservationSpotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReservationFactory {
    @Autowired
    private ReservationSpotFactory reservationSpotFactory;
    @Autowired
    private ReservationSpotRepository reservationSpotRepository;
    @Autowired
    private ReservationRepository reservationRepository;

    public Reservation create(ReservationDto reservationDto) {

        if (reservationDto == null) {
            throw new CantCreateReservationException("Reservation Dto is null");
        }

        return createReservation(reservationDto);
    }

    private Reservation createReservation(ReservationDto dto) {
        return switch (dto.getSpotType()) {
            case LOUNGE -> createLoungeReservation(dto);
            case BAR ->  null; //createBarReservation(dto);
            case TABLE -> null;
        };
    }

    /*private Reservation createBarReservation(ReservationDto dto) {
        LocalDate date = LocalDate.parse(dto.getReservationDate());
        LocalTime start = LocalTime.parse(dto.getReservationStart());
        LocalTime end = start.plus(Duration.ofHours(dto.getReservationLength()));
        List<ReservationSpot> spotList = reservationSpotRepository.findBySpotType(SpotType.BAR);
        List<Reservation> reservationList = reservationRepository
                .findByReservationDateAndReservationStartBetween(date, start, end);

        List<ReservationSpot> bookedSpots = reservationList.stream()
                .map(reservation -> reservation.getReservedSpot())
                .collect(Collectors.toList());

        List<ReservationSpot> freeSpots = spotList.stream()
                .filter(spot -> !bookedSpots.contains(spot))
                .collect(Collectors.toList());

        if (freeSpots.size() >= dto.getGuestNumber()) {
            List<ReservationSpot> reservedSpots = new ArrayList<>();
            for (int x = 0; x < dto.getGuestNumber(); x++) {
                reservedSpots.add(freeSpots.get(x));
            }
            return new Reservation(reservedSpots, start, dto.getReservationLength(), date);

        } else throw new IllegalStateException("not enough free spots");

    }*/

    private Reservation createLoungeReservation(ReservationDto dto) {
        LocalDate date = LocalDate.parse(dto.getReservationDate());
        LocalTime start = LocalTime.parse(dto.getReservationStart());
        LocalTime end = start.plus(Duration.ofHours(dto.getReservationLength()));
        List<ReservationSpot> spotList = reservationSpotRepository.findBySpotType(SpotType.LOUNGE);
        List<Reservation> reservationList = reservationRepository
                .findByReservationDateAndReservationStartBetween(date, start, end);


        ReservationSpot freeSpot = spotList.stream().filter(spot -> !reservationList.stream()
                .map(reservation -> reservation.getReservedSpot())
                .collect(Collectors.toList())
                .contains(spot))
                .filter(spot -> spot.getSeatsNumber() >= dto.getGuestNumber())
                .findFirst().orElseThrow(() -> new IllegalStateException("No free spots"));

        return new Reservation(freeSpot, start, dto.getReservationLength(), date);
    }



    public Reservation update(ReservationDto reservationDto, Long id) {
        if (reservationDto == null || id == null) {
            throw new CantUpdateReservationException("Reservation Dto is null or id is null");
        }
        Reservation reservation = reservationRepository.findById(id)
                .orElseThrow(() -> new IllegalStateException("Can't find reservation"));
        Reservation toUpdate = create(reservationDto);
        toUpdate.setUser(reservation.getUser());
        toUpdate.setId(reservation.getId());
        return toUpdate;
    }
}