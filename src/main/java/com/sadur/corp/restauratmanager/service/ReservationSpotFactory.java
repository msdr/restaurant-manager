package com.sadur.corp.restauratmanager.service;

import com.sadur.corp.restauratmanager.gui.dto.ReservationSpotDto;
import com.sadur.corp.restauratmanager.model.reservation.*;
import com.sadur.corp.restauratmanager.repository.ReservationSpotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReservationSpotFactory {

	@Autowired
	private ReservationSpotRepository reservationSpotRepository;

	public ReservationSpotFactory() {
	}

	public ReservationSpot create(int seatsNumber) {
		if (seatsNumber == 1) {
			return new Bar();
		} else if (seatsNumber > 1 && seatsNumber <= 4){
			return new Table(seatsNumber);
		}else if (seatsNumber > 4 && seatsNumber < 9){
				return new Lounge(seatsNumber);
		}
		else throw new IllegalStateException(String.format("Can't create spot with %s seats",seatsNumber));
	}

	public ReservationSpotDto createDto(int seatsNumber) {
		if (seatsNumber == 1) {
			return new ReservationSpotDto(seatsNumber, SpotType.BAR);
		} else if (seatsNumber > 1 && seatsNumber <= 4){
			return new ReservationSpotDto(seatsNumber, SpotType.TABLE);
		}else if (seatsNumber > 4 && seatsNumber < 9){
			return new ReservationSpotDto(seatsNumber, SpotType.LOUNGE);
		}
		else throw new IllegalStateException(String.format("Can't create spot with %s seats",seatsNumber));
	}

	public List<ReservationSpot> getAllSpots(){
		return reservationSpotRepository.findAll();
	}

}
