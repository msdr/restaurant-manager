package com.sadur.corp.restauratmanager.service;

import com.sadur.corp.restauratmanager.model.Reservation;
import com.sadur.corp.restauratmanager.model.menu.MenuItem;
import com.sadur.corp.restauratmanager.model.menu.Order;
import com.sadur.corp.restauratmanager.repository.MenuItemRepository;
import com.sadur.corp.restauratmanager.repository.OrderRepository;
import com.sadur.corp.restauratmanager.repository.ReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class OrderService {
	@Autowired
	private OrderRepository orderRepository;
	@Autowired
	private MenuItemRepository menuItemRepository;
	@Autowired
	private ReservationRepository reservationRepository;

	public List<Order> getAllOrders() {
		return orderRepository.findAll();
	}

	public Order createOrder(List<Long> ids) {
		List<MenuItem> items = ids.stream() //Stream<Long>
				.map(id -> menuItemRepository.findById(id)) //Stream<Optional<MenuItem>>
				.flatMap(item -> item.stream()) //Stream<MenuItem>
				.filter(item -> Objects.nonNull(item))//Stream<MenuItem>
				.collect(Collectors.toList()); // List<MenuItem>

		BigDecimal orderValue = items.stream()
				.map(item -> item.getValue())
				.reduce(BigDecimal.ZERO, BigDecimal::add);

		Order order =  new Order(items, orderValue);

		return orderRepository.save(order);
	}


	public Reservation addOrderToReservation(Long orderId, Long reservationId){
		Optional<Order> order = orderRepository.findById(orderId);
		Optional<Reservation> reservation = reservationRepository.findById(reservationId);

		if(order.isPresent() && reservation.isPresent()) {
			Reservation reservationToUpdate = reservation.get();
			reservationToUpdate.setOrder(order.get());
			return reservationRepository.save(reservationToUpdate);
		}

		throw new IllegalStateException("Order or reservation not found");
	}
}
