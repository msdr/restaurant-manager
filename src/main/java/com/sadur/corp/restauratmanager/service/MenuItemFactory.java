package com.sadur.corp.restauratmanager.service;


import com.sadur.corp.restauratmanager.gui.dto.MenuItemEditDto;
import com.sadur.corp.restauratmanager.model.dto.MenuItemDto;
import com.sadur.corp.restauratmanager.model.menu.*;
import org.springframework.stereotype.Service;

@Service
public class MenuItemFactory {

    public MenuItem create(MenuItemDto menuItemDto) {
        return new MenuItem(menuItemDto.getName(), menuItemDto.getValue(),
                Currency.valueOf(menuItemDto.getCurrency()));
    }

    public MenuItem create(MenuItemEditDto dto) {
        MenuItem menuItem = new MenuItem();
        menuItem.setName(dto.getName());
        menuItem.setValue(dto.getValue());
        menuItem.setCurrency(Currency.getCurrency(dto.getCurrency()));
        menuItem.setItemType(ItemType.valueOf(dto.getItemType()));
        menuItem.setPromotionBundle(getPromoBundle(dto));
        return menuItem;
    }

    private PromotionBundle getPromoBundle(MenuItemEditDto dto) {
        PromotionBundle promotionBundle = new PromotionBundle();
        promotionBundle.setStandard(dto.isStandard());
        promotionBundle.setExtra(dto.isExtra());
        promotionBundle.setPremium(dto.isPremium());
        return promotionBundle;
    }
}
