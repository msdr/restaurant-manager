package com.sadur.corp.restauratmanager.service;

import com.sadur.corp.restauratmanager.gui.dto.UserDto;
import com.sadur.corp.restauratmanager.model.user.CustomUserDetails;
import com.sadur.corp.restauratmanager.model.user.User;
import com.sadur.corp.restauratmanager.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return new CustomUserDetails(getUser(email));
    }

    private User getUser(String eMail) {
        return userRepository.findByEmail(eMail).orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }

    public void createUser(UserDto userDto) {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        User user = new User(userDto.getEmail(), bCryptPasswordEncoder.encode(userDto.getPassword()));
        userRepository.save(user);
    }
}
