package com.sadur.corp.restauratmanager.service;


import com.sadur.corp.restauratmanager.gui.dto.MenuItemDto;
import com.sadur.corp.restauratmanager.gui.dto.MenuItemEditDto;
import com.sadur.corp.restauratmanager.model.menu.Currency;
import com.sadur.corp.restauratmanager.model.menu.Menu;
import com.sadur.corp.restauratmanager.model.menu.MenuItem;
import com.sadur.corp.restauratmanager.repository.CartRepository;
import com.sadur.corp.restauratmanager.repository.MenuItemRepository;
import com.sadur.corp.restauratmanager.repository.MenuRepository;
import com.sadur.corp.restauratmanager.service.cart.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MenuItemService {

    @Autowired
    private MenuItemFactory menuItemFactory;
    @Autowired
    private MenuItemRepository menuItemRepository;
    @Autowired
    private MenuItemMapper menuItemMapper;
    @Autowired
    private MenuRepository menuRepository;
    @Autowired
    private CartRepository cartRepository;
    @Autowired
    private CartService cartService;


    public MenuItem create(MenuItemEditDto menuItemDto) {
        MenuItem menuItem = menuItemFactory.create(menuItemDto);
        return menuItemRepository.save(menuItem);
    }

    public MenuItem getOneMenuItem(Long menuId){
        return menuItemRepository.findById(menuId)
                .orElseThrow(() -> new IllegalStateException("Can't find item"));
    }

    public List<MenuItemDto> getMenuItemsList(){
        return menuItemRepository.findAll().stream()
                .map(menuItem -> menuItemMapper.mapFrom(menuItem))
                .collect(Collectors.toList());
    }

    public MenuItemEditDto getMenuEditItem(Long id){
        return menuItemRepository.findById(id)
                .map(menuItem -> menuItemMapper.mapToEditDto(menuItem))
                .orElseThrow(() -> new IllegalStateException("can't find item"));
    }

   /* public void addItemToTheCart (Long id){

        cartRepository.save menuItemRepository.findById(id);
    }*/

    public void update(MenuItemEditDto dto){
        MenuItem menuItemToUpdate = menuItemRepository.findById(dto.getId())
                .get();
        menuItemToUpdate.setName(dto.getName());
        menuItemToUpdate.setValue(dto.getValue());
        menuItemToUpdate.setCurrency(Currency.valueOf(dto.getCurrency()));
        menuItemToUpdate.setMenus(addMenus(dto.getAssignedMenuId()));
        menuItemRepository.save(menuItemToUpdate);
    }

    private List<Menu> addMenus(Long menuId){
        List<Menu> menus = new ArrayList<>();
        menuRepository.findById(menuId)
                .ifPresent(menu -> menus.add(menu));
        return menus;
    }
/*
    public List<MenuItem> getMenuItemsForOrder(Long orderId){

        return menuItemRepository.getMenuItemsByOrder(orderId);

    }
*/
}