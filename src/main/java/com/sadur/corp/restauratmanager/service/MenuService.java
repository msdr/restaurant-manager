package com.sadur.corp.restauratmanager.service;

import com.sadur.corp.restauratmanager.model.dto.MenuDto;
import com.sadur.corp.restauratmanager.model.dto.MenuItemDto;
import com.sadur.corp.restauratmanager.model.dto.SimpleMenuDto;
import com.sadur.corp.restauratmanager.model.menu.Currency;
import com.sadur.corp.restauratmanager.model.menu.Menu;
import com.sadur.corp.restauratmanager.model.menu.MenuItem;
import com.sadur.corp.restauratmanager.repository.MenuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class MenuService {

    @Autowired
    private MenuRepository menuRepository;
    @Autowired
    private MenuImporter menuImporter;
    @Autowired
    private MenuItemFactory menuItemFactory;

    public Menu create(MenuDto menuDto) {
        Menu menu = new Menu(menuDto);
        return menuRepository.save(menu);
    }

    public List<SimpleMenuDto> getAllMenus() {
        return menuRepository.findByActiveTrue().stream()
                .map(menu -> new SimpleMenuDto(menu.getId(), menu.getName(), menu.isAvailable()))
                .collect(Collectors.toList());
    }

    public MenuDto findById(Long id) {
        return menuRepository.findById(id)
                .map(menu -> new MenuDto(menu))
                .orElseThrow(() -> new IllegalStateException("can't find menu"));
    }

    public Long updateMenu(MenuDto menuDto) {
        Menu menu = menuRepository.findById(menuDto.getId())
                .orElseThrow(() -> new IllegalStateException("can't find menu"));
        menu.setName(menuDto.getName());
        menu.setAvailableFrom(LocalDate.parse(menuDto.getAvailableFrom()));
        menu.setAvailableTill(LocalDate.parse(menuDto.getAvailableTill()));
        menu.setAvailable(menuDto.isAvailable());
        menuRepository.save(menu);
        return menu.getId();
    }

    public void changeMenuActiveStatus(Long id) {

        menuRepository.findById(id)
                .ifPresent(menu -> {
                    menu.setActive(!menu.isActive());
                    menuRepository.save(menu);
                });
    }

    public Menu importMenuFromFile(File menuFile, Long menuId) throws IOException {
        if (Objects.nonNull(menuFile) && Objects.nonNull(menuId)) {

            List<MenuItemDto> importedMenuItems;

            if (menuFile.getName().contains(".csv")) {
                importedMenuItems = menuImporter.readFromCsvFile(menuFile);
            } else if (menuFile.getName().contains(".json")) {
                importedMenuItems = menuImporter.readFromJsonFile(menuFile);
            } else throw new IllegalStateException("wrong type of menu file. Cant create menu in DB");

            Menu menu = menuRepository.findById(menuId)
                    .orElseThrow(() -> new IllegalStateException("Can't find menu"));

            List<MenuItem> items = menu.getMenuItems();

            List<MenuItem> updatedMenuItems = addOrUpdateItemList(items, importedMenuItems);

            menu.setMenuItems(updatedMenuItems);

            return menuRepository.save(menu);
        }
        throw new IllegalStateException("Cant import data");
    }

    public List<Menu> getAvailableMenu() {
        return menuRepository.findByAvailableTrue();
    }

    private List<MenuItem> addOrUpdateItemList(List<MenuItem> menuItems, List<MenuItemDto> importedMenuItems) {
        List<MenuItem> resultList = new ArrayList<>();

        List<MenuItem> updatedMenuItems = importedMenuItems.stream()
                .filter(itemDto -> getNamesFromMenuItems(menuItems).contains(itemDto.getName()))
                .map(itemDto -> mapToMenuItem(menuItems, itemDto))
                .filter(menuItem -> Objects.nonNull(menuItem))
                .collect(Collectors.toList());

        List<MenuItem> menuItemsNotOnList = importedMenuItems.stream()
                .filter(itemDto -> !getNamesFromMenuItems(menuItems).contains(itemDto.getName()))
                .map(itemDto -> menuItemFactory.create(itemDto))
                .collect(Collectors.toList());

        if (updatedMenuItems.size() == 0) {
            resultList.addAll(menuItems);
        }
        resultList.addAll(updatedMenuItems);
        resultList.addAll(menuItemsNotOnList);

        return resultList;
    }

    private List<String> getNamesFromMenuItems(List<MenuItem> menuItems) {
        return menuItems.stream().map(menuItem -> menuItem.getName()).collect(Collectors.toList());
    }

    private MenuItem mapToMenuItem(List<MenuItem> menuItems, MenuItemDto itemDto) {
        return menuItems.stream()
                .filter(item -> item.getName().equals(itemDto.getName()))
                .findFirst()
                .map(item -> mapToMenuItem(item, itemDto))
                .orElse(null);
    }

    private MenuItem mapToMenuItem(MenuItem menuItem, MenuItemDto itemDto) {
        menuItem.setValue(itemDto.getValue());
        menuItem.setCurrency(Currency.getCurrency(itemDto.getCurrency()));
        return menuItem;
    }
}
    //TODO
    //dto
    //dto na front
    //update dat
    //controller
        //rest (CRUD)
        //widoki
            //admin
            //user - only active menus

