package com.sadur.corp.restauratmanager.service;

import com.sadur.corp.restauratmanager.model.menu.Menu;
import com.sadur.corp.restauratmanager.repository.MenuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class MenuScheduler {

    @Autowired
    private MenuRepository menuRepository;

    @Scheduled(cron ="${menu.checker.frequency}")
    public void menuChecker() {
        menuRepository.findByAvailableTrue()
                .stream()
                .filter(menu -> !isAvailable(menu))
                .map(menu -> setAvailableFlag(menu))
                .forEach(menu -> menuRepository.save(menu));
    }

    private Menu setAvailableFlag(Menu menu) {
        menu.setAvailable(false);
        return menu;
    }

    private boolean isAvailable(Menu menu) {
        LocalDate now = LocalDate.now();

        if (menu.getAvailableFrom() != null && menu.getAvailableTill() != null) {
            if (menu.getAvailableFrom().isEqual(now) || menu.getAvailableTill().isEqual(now)) {
                return true;
            }
            if (menu.getAvailableTill().isBefore(now)) {
                return false;
            }
            if (menu.getAvailableFrom().isAfter(now)) {
                return false;
            }
        }

        if (menu.getAvailableFrom() == null && menu.getAvailableTill() != null) {
            return menu.getAvailableTill().isEqual(now) || menu.getAvailableTill().isAfter(now);
        }

        if (menu.getAvailableTill() == null && menu.getAvailableFrom() != null) {
            return !menu.getAvailableFrom().isAfter(now);
        }

        if (menu.getAvailableTill() == null) {
            return true;
        }

        return menu.getAvailableTill().isAfter(now);
    }


}
