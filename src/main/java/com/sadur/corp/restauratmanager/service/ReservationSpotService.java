package com.sadur.corp.restauratmanager.service;

import com.sadur.corp.restauratmanager.model.reservation.ReservationSpot;
import com.sadur.corp.restauratmanager.repository.ReservationSpotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReservationSpotService {

    @Autowired
    private ReservationSpotFactory reservationSpotFactory;
    @Autowired
    private ReservationSpotRepository reservationSpotRepository;


    public ReservationSpot newSpot(int seatsNumber){
        ReservationSpot spot = reservationSpotFactory.create(seatsNumber);
        return reservationSpotRepository.save(spot);
    }

    public List<ReservationSpot> getAllSpots(){
        return reservationSpotRepository.findAll();
    }
}
