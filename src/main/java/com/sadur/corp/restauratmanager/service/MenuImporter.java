package com.sadur.corp.restauratmanager.service;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.sadur.corp.restauratmanager.model.dto.MenuItemDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class MenuImporter {

    private static final Logger LOGGER = LoggerFactory.getLogger(MenuImporter.class);

    public List<MenuItemDto> readFromJsonFile(File jsonInputFile) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            ObjectReader objectReader = objectMapper.readerFor(MenuItemDto[].class);

            return crateAndValidateList(objectReader.readValue(jsonInputFile));
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return new ArrayList<>();
    }

    public List<MenuItemDto> readFromCsvFile(File csvInputFile) throws IOException {
        CsvMapper csvMapper = new CsvMapper();

        CsvSchema.Builder csvSchemaBuilder = CsvSchema.builder();
        List<String> columns = Arrays.asList("name", "value", "currency");
        columns.forEach(column -> csvSchemaBuilder.addColumn(column));
        CsvSchema csvSchema = csvSchemaBuilder.build().withHeader();
        ObjectReader objectReader = csvMapper.readerFor(MenuItemDto.class).with(csvSchema);

        MappingIterator<MenuItemDto> mappingIterator = objectReader.readValues(csvInputFile);
        return mappingIterator.readAll();
    }

    private List<MenuItemDto> crateAndValidateList(MenuItemDto[] objectArray) {
        List<MenuItemDto> dtos = Arrays.asList(objectArray);
        dtos.forEach(dto -> checkForNulls(dto));
        return dtos;
    }

    private void checkForNulls(MenuItemDto menuItemDto) {
        if (menuItemDto.getName() == null) {
            throw new IllegalStateException("name field is missing");
        }
        if (menuItemDto.getValue() == null) {
            throw new IllegalStateException("value field is missing");
        }
        if (menuItemDto.getCurrency() == null) {
            throw new IllegalStateException("currency field is missing");
        }
    }
}
