package com.sadur.corp.restauratmanager.service.cart;


import com.sadur.corp.restauratmanager.gui.dto.CartDto;
import com.sadur.corp.restauratmanager.gui.dto.MenuItemDto;
import com.sadur.corp.restauratmanager.model.cart.Cart;
import com.sadur.corp.restauratmanager.model.cart.CartStatus;
import com.sadur.corp.restauratmanager.service.MenuItemMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CartMapper {

	@Autowired
	private MenuItemMapper menuItemMapper;


	public CartDto mapFromCart(Cart cart) {
		if (cart.getCartStatus() != CartStatus.ABANDONED) {
			CartDto cartDto = new CartDto();
			cartDto.setMenuItems(mapMenuItems(cart));
			cartDto.setCartValue(cart.getCartValue());
			cartDto.setEmpty(cart.getMenuItems().isEmpty());
			return cartDto;
		}
		throw new IllegalStateException("Cart Status Abondoned");
	}

	private List<MenuItemDto> mapMenuItems(Cart cart) {
		return cart.getMenuItems().stream()
			.map(menuItem -> menuItemMapper.mapFrom(menuItem)).collect(Collectors.toList());
	}
}
