package com.sadur.corp.restauratmanager.controller;

import com.sadur.corp.restauratmanager.model.reservation.ReservationSpot;
import com.sadur.corp.restauratmanager.service.ReservationSpotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
//TODO dodać metody zmiany/dodawanie etc ze spotami


@RestController
public class SpotController {

    @Autowired
    private ReservationSpotService reservationSpotService;

    @PostMapping(value = "/spot/new")
    public ReservationSpot createSpot(@RequestParam int seatsNumber){
       return reservationSpotService.newSpot(seatsNumber) ;
    }

    @GetMapping(value = "/spot/all")
    public List<ReservationSpot> getAllSpots(){
        return reservationSpotService.getAllSpots();
    }

    //@PutMapping(value = "/spot/update")
    //public ReservationSpot updateSpot(@RequestParam int seatsNumber, @RequestParam SpotType spotType, Long id){
    //

}
