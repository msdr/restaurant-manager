package com.sadur.corp.restauratmanager.controller;

import com.sadur.corp.restauratmanager.model.Reservation;
import com.sadur.corp.restauratmanager.model.menu.Order;
import com.sadur.corp.restauratmanager.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping(value = "/order/all")
    public List<Order> getOrders(){
        return orderService.getAllOrders();
    }

    @PostMapping(value = "/order/new")
    public Order addOrder(@RequestParam List<Long> ids){
        return orderService.createOrder(ids);
    }

    @PutMapping(value = {"/order/addToReservation", "/reservation/addOrder"})
    public Reservation addToReservation(@RequestParam  Long orderId, @RequestParam Long reservationId){
        return orderService.addOrderToReservation(orderId, reservationId);
    }
}
