package com.sadur.corp.restauratmanager.controller;
import com.sadur.corp.restauratmanager.model.Reservation;
import com.sadur.corp.restauratmanager.model.dto.ReservationDto;
import com.sadur.corp.restauratmanager.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RestaurantController {

    @Autowired
    private ReservationService reservationService;

   // @RequestMapping(method = RequestMethod.POST) to samo
    @PostMapping(value = "/reservation/new")
    public Reservation addNewReservation(@RequestBody ReservationDto reservationDto){
       return reservationService.newReservation(reservationDto);
    }

   @GetMapping(value = "/reservation/all")
    public List<Reservation> getReservations(){
        return reservationService.getAllReservations();
   }

   @PutMapping(value = "/reservation/update")
    public Reservation updateReservation(@RequestBody ReservationDto reservationDto, @RequestParam Long id){
        return reservationService.updateReservation(reservationDto, id);
   }

}
