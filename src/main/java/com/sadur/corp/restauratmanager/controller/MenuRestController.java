package com.sadur.corp.restauratmanager.controller;

import com.sadur.corp.restauratmanager.model.menu.MenuItem;
import com.sadur.corp.restauratmanager.service.MenuItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class MenuRestController {

    @Autowired
    private MenuItemService menuItemService;
/*
    @PostMapping(value = "/menu_item/new")
    public MenuItem addNewMenuItem(@RequestBody MenuItemDto menuItemDto){
     return menuItemService.newMenuItem(menuItemDto); }
*/
    @GetMapping(value = "/menu_item/all")
    public List<com.sadur.corp.restauratmanager.gui.dto.MenuItemDto> getMenuItems() {
        return menuItemService.getMenuItemsList();
    }

    @GetMapping(value = "/menu_item/{id}")
    public MenuItem idVariable(@PathVariable Long id){
            return menuItemService.getOneMenuItem(id);
        }

}



