package com.sadur.corp.restauratmanager.gui;

import com.sadur.corp.restauratmanager.gui.dto.MenuItemEditDto;
import com.sadur.corp.restauratmanager.model.dto.IdDto;
import com.sadur.corp.restauratmanager.service.MenuItemService;
import com.sadur.corp.restauratmanager.service.MenuService;
import com.sadur.corp.restauratmanager.service.cart.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MenuItemController {

    @Autowired
    private MenuItemService menuItemService;
    @Autowired
    private MenuService menuService;
    @Autowired
    private CartService cartService;

    @GetMapping(value = "/menuItems")
    public ModelAndView getAllMenuItems() {
        ModelAndView mov = new ModelAndView("menuItem/list");
        mov.addObject("menuItems", menuItemService.getMenuItemsList());
        return mov;
    }

    @GetMapping(value = "/menuItems/details")
    public ModelAndView getAllDetailedMenuItems(Authentication authentication) {
        ModelAndView mov = new ModelAndView("menuItem/detailed-list");
        mov.addObject("menuItems", menuItemService.getMenuItemsList());
        mov.addObject("user", authentication.getName());
        mov.addObject("cart", cartService.getDto(authentication));
        mov.addObject("form", new IdDto());
        return mov;
    }

    @GetMapping(value = "/menuItem/create")
    public ModelAndView create(){
        ModelAndView mov = new ModelAndView("menuItem/create");
        mov.addObject("form", new MenuItemEditDto());
        mov.addObject("menus", menuService.getAllMenus());
        return mov;
    }

    @PostMapping(value = "/menuItem/create")
    public String createFrom(@ModelAttribute MenuItemEditDto dto){
        Long id = menuItemService.create(dto).getId();
        return "redirect:/menuItem/edit?id=" + id;
    }

    @GetMapping(value = "/menuItem/edit")
    public ModelAndView edit(@RequestParam Long id){
        ModelAndView mov = new ModelAndView("menuItem/edit");
        mov.addObject("form", menuItemService.getMenuEditItem(id));
        mov.addObject("menus", menuService.getAllMenus());
        return mov;
    }
    @PostMapping(value = "/menuItem/edit")
    public String update(@ModelAttribute MenuItemEditDto dto){
        menuItemService.update(dto);
        return "redirect:/menuItem/edit?id=" + dto.getId();
    }
}