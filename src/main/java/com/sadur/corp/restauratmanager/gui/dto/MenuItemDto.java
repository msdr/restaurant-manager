package com.sadur.corp.restauratmanager.gui.dto;

import com.sadur.corp.restauratmanager.model.menu.ItemType;
import com.sadur.corp.restauratmanager.model.menu.PromotionBundle;

public class MenuItemDto {

    private Long id;
    private String name;
    private String price;
    private boolean assignedToMenu;
    private ItemType itemType;
    private PromotionBundle promotionBundle;

    public MenuItemDto() {
    }

    public MenuItemDto(Long id, String name, String price, boolean assignedToMenu) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.assignedToMenu = assignedToMenu;
    }

    public MenuItemDto(Long id, String name, String price, boolean assignedToMenu, ItemType itemType, PromotionBundle promotionBundle) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.assignedToMenu = assignedToMenu;
        this.itemType = itemType;
        this.promotionBundle = promotionBundle;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public boolean isAssignedToMenu() {
        return assignedToMenu;
    }

    public void setAssignedToMenu(boolean assignedToMenu) {
        this.assignedToMenu = assignedToMenu;
    }

    public ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public PromotionBundle getPromotionBundle() {
        return promotionBundle;
    }

    public void setPromotionBundle(PromotionBundle promotionBundle) {
        this.promotionBundle = promotionBundle;
    }

}
