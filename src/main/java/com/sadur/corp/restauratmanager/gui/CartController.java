package com.sadur.corp.restauratmanager.gui;


import com.sadur.corp.restauratmanager.model.dto.IdDto;
import com.sadur.corp.restauratmanager.service.cart.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class CartController {

    @Autowired
    private CartService cartService;

    @PostMapping(value = "/menuItem/addToCart")
    public String addToCart(@ModelAttribute IdDto idDto, Authentication authentication) {
        cartService.addToCart(authentication, idDto.getId());
        return "redirect:/menuItems/details";
    }

    @PostMapping(value = "/menuItem/removeFromCart")
    public String removeFromCart(@ModelAttribute IdDto idDto, Authentication authentication) {
        cartService.removeFromCart(authentication, idDto.getId());
        return "redirect:/menuItems/details";
    }


}
