package com.sadur.corp.restauratmanager.gui.dto;

import java.math.BigDecimal;

public class MenuItemEditDto {

    private Long id;
    private String name;
    private BigDecimal value;
    private String currency;
    private Long assignedMenuId;
    private String itemType;
    private boolean standard;
    private boolean extra;
    private boolean premium;

    public MenuItemEditDto() {
    }

    public MenuItemEditDto(Long id, String name, BigDecimal value,
                           String currency, Long assignedMenuId) {
        this.id = id;
        this.name = name;
        this.value = value;
        this.currency = currency;
        this.assignedMenuId = assignedMenuId;
    }

    public MenuItemEditDto(Long id, String name, BigDecimal value, String currency,
                           Long assignedMenuId, String itemType, boolean standard,
                           boolean extra, boolean premium) {
        this.id = id;
        this.name = name;
        this.value = value;
        this.currency = currency;
        this.assignedMenuId = assignedMenuId;
        this.itemType = itemType;
        this.standard = standard;
        this.extra = extra;
        this.premium = premium;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Long getAssignedMenuId() {
        return assignedMenuId;
    }

    public void setAssignedMenuId(Long assignedMenuId) {
        this.assignedMenuId = assignedMenuId;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public boolean isStandard() {
        return standard;
    }

    public void setStandard(boolean standard) {
        this.standard = standard;
    }

    public boolean isExtra() {
        return extra;
    }

    public void setExtra(boolean extra) {
        this.extra = extra;
    }

    public boolean isPremium() {
        return premium;
    }

    public void setPremium(boolean premium) {
        this.premium = premium;
    }
}