package com.sadur.corp.restauratmanager.gui.dto;

import java.time.LocalDate;
import java.time.LocalTime;

public class ReservationViewDto {

    private Long id;
    private LocalTime reservationStart;
    private int reservationTime;
    private int guestNumber;
    private LocalDate reservationDate;

    public ReservationViewDto(Long id, LocalTime reservationStart, int reservationTime, int guestNumber, LocalDate reservationDate) {
        this.id = id;
        this.reservationStart = reservationStart;
        this.reservationTime = reservationTime;
        this.guestNumber = guestNumber;
        this.reservationDate = reservationDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalTime getReservationStart() {
        return reservationStart;
    }

    public void setReservationStart(LocalTime reservationStart) {
        this.reservationStart = reservationStart;
    }

    public int getReservationLength() {
        return reservationTime;
    }

    public void setReservationTime(int reservationTime) {
        this.reservationTime = reservationTime;
    }

    public int getGuestNumber() {
        return guestNumber;
    }

    public void setGuestNumber(int guestNumber) {
        this.guestNumber = guestNumber;
    }

    public LocalDate getReservationDate() {
        return reservationDate;
    }

    public void setReservationDate(LocalDate reservationDate) {
        this.reservationDate = reservationDate;
    }
}
