package com.sadur.corp.restauratmanager.gui.dto;

import com.sadur.corp.restauratmanager.model.reservation.SpotType;

public class ReservationSpotDto {

    private int seatsNumber;
    private SpotType spotType;

    public ReservationSpotDto() {
    }

    public ReservationSpotDto(int seatsNumber, SpotType spotType) {
        this.seatsNumber = seatsNumber;
        this.spotType = spotType;
    }

    public int getSeatsNumber() {
        return seatsNumber;
    }

    public void setSeatsNumber(int seatsNumber) {
        this.seatsNumber = seatsNumber;
    }

    public SpotType getSpotType() {
        return spotType;
    }

    public void setSpotType(SpotType spotType) {
        this.spotType = spotType;
    }
}
