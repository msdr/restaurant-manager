package com.sadur.corp.restauratmanager.gui.dto;

import com.sadur.corp.restauratmanager.model.menu.MenuItem;

import java.math.BigDecimal;

public class MenuItemViewDto {

    private long id;
    private String name;
    private BigDecimal value;
    private String currency;

    public MenuItemViewDto(long id, String name, BigDecimal value, String currency) {
        this.id = id;
        this.name = name;
        this.value = value;
        this.currency = currency;
    }

    public MenuItemViewDto(MenuItem menuItem) {
        this.id = menuItem.getId();
        this.name = menuItem.getName();
        this.value = menuItem.getValue();
        this.currency = menuItem.getCurrency().name();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
