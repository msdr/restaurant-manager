package com.sadur.corp.restauratmanager.gui;

import com.sadur.corp.restauratmanager.gui.dto.UserDto;
import com.sadur.corp.restauratmanager.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("/user/new")
    public String addNewUser(@ModelAttribute UserDto userDto) {
        userService.createUser(userDto);
        return "redirect:/login";
    }

    @GetMapping("/user/new")
    public ModelAndView createUser(){
        ModelAndView mav = new ModelAndView("user/create-new");
        mav.addObject("form", new UserDto());
        return mav;
    }

}
