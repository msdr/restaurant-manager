package com.sadur.corp.restauratmanager.gui;

import com.sadur.corp.restauratmanager.model.dto.IdDto;
import com.sadur.corp.restauratmanager.model.dto.MenuDto;
import com.sadur.corp.restauratmanager.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MenuViewController {

    @Autowired
    private MenuService menuService;

    @GetMapping(value = "/menus")
    public ModelAndView getAllMenus() {

        ModelAndView modelAndView = new ModelAndView("menus");
        modelAndView.addObject("menus", menuService.getAllMenus());
        modelAndView.addObject("form", new IdDto());
        return modelAndView;

    }

    @GetMapping(value = "/menu/add")
    public ModelAndView addMenu() {
        ModelAndView modelAndView = new ModelAndView("menu-new");
        modelAndView.addObject("form", new MenuDto());
        return modelAndView;
    }

    @PostMapping(value = "/menu/add")
    public String addMenu(@ModelAttribute MenuDto menuDto) {
        menuService.create(menuDto);
        return "redirect:/menus";
    }

    @GetMapping(value = "/menu/edit")
    public ModelAndView editMenu(@RequestParam Long id) {
        ModelAndView modelAndView = new ModelAndView("menu-edit");
        modelAndView.addObject("form", menuService.findById(id));
        return modelAndView;
    }

    @PutMapping(value = "/menu/edit")
    public String editMenu(@ModelAttribute MenuDto menuDto) {
        Long id = menuService.updateMenu(menuDto);
        return "redirect:/menu/edit?id=" + id;
    }

    @PutMapping(value = "/menu/change-status")
    public String changeMenuStatus(@ModelAttribute IdDto idDto) {
        menuService.changeMenuActiveStatus(idDto.getId());
        return "redirect:/menus";
    }
}
