package com.sadur.corp.restauratmanager.gui;

import com.sadur.corp.restauratmanager.model.dto.ReservationDto;
import com.sadur.corp.restauratmanager.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ReservationController {

    @Autowired
    private ReservationService reservationService;

    @GetMapping(value = "/")
    public ModelAndView allReservations() {
        ModelAndView mav = new ModelAndView("reservations/list");
        mav.addObject("reservations", reservationService.getAllReservationsForView());
        return mav;
    }

    @GetMapping(value = "/detailedView")
    public ModelAndView detailedReservation(@RequestParam long id) {
        ModelAndView mav = new ModelAndView("detailedView");
        mav.addObject("reservation", reservationService.getDetailedView(id));
        return mav;
    }

    @GetMapping(value = "/reservations/create")
    public ModelAndView createReservation() {
        ModelAndView mav = new ModelAndView("reservations/create");
        mav.addObject("form", new ReservationDto());
        return mav;
    }

    @PostMapping(value = "reservations/create")
    public String create(@ModelAttribute ReservationDto reservationDto){
        reservationService.newReservation(reservationDto);
        return "redirect:/";
    }
}
