package com.sadur.corp.restauratmanager.gui;


import com.sadur.corp.restauratmanager.service.ReservationSpotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ReservationSpotController {

    @Autowired
    ReservationSpotService reservationSpotService;

    @GetMapping(value = "/reservationSpots")
    public ModelAndView AllSpots (){
        ModelAndView mav = new ModelAndView("reservationSpot/list");
        mav.addObject("reservationSpot", reservationSpotService.getAllSpots());
        return mav;
    }

//    @PostMapping(value = "/reservationSpots/create")
//    public String create (@ModelAttribute int seatsNumber){
//        ModelAndView mav = new ModelAndView("reservationSpot/create");
//        reservationSpotService.newSpot(seatsNumber);
//        return "redirect:/reservationSpots";
//    }

}
