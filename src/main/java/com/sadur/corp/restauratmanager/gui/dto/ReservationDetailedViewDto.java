package com.sadur.corp.restauratmanager.gui.dto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public class ReservationDetailedViewDto extends ReservationViewDto{

    private Long spotId;
    private String spotType;
    private List<MenuItemViewDto> menuItems;
    private BigDecimal value;
    private String comment;
    private String name;

    public ReservationDetailedViewDto(Long id, LocalTime reservationStart, int reservationTime,
                                      int guestNumber, LocalDate reservationDate, Long spotId,
                                      String spotType, List<MenuItemViewDto> menuItems,
                                      BigDecimal value, String comment, String name) {
        super(id, reservationStart, reservationTime, guestNumber, reservationDate);
        this.spotId = spotId;
        this.spotType = spotType;
        this.menuItems = menuItems;
        this.value = value;
        this.comment = comment;
        this.name = name;
    }

    public Long getSpotId() {
        return spotId;
    }

    public void setSpotId(Long spotId) {
        this.spotId = spotId;
    }

    public String getSpotType() {
        return spotType;
    }

    public void setSpotType(String spotType) {
        this.spotType = spotType;
    }

    public List<MenuItemViewDto> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<MenuItemViewDto> menuItems) {
        this.menuItems = menuItems;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
