package com.sadur.corp.restauratmanager.gui.dto;

import java.math.BigDecimal;
import java.util.List;

public class CartDto {

    private List<MenuItemDto> menuItems;
    private BigDecimal cartValue;
    private boolean empty;

    public CartDto() {
    }

    public CartDto(List<MenuItemDto> menuItems, BigDecimal cartValue, boolean empty) {
        this.menuItems = menuItems;
        this.cartValue = cartValue;
        this.empty = empty;
    }

    public List<MenuItemDto> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<MenuItemDto> menuItems) {
        this.menuItems = menuItems;
    }

    public BigDecimal getCartValue() {
        return cartValue;
    }

    public void setCartValue(BigDecimal cartValue) {
        this.cartValue = cartValue;
    }

    public boolean isEmpty() {
        return empty;
    }

    public void setEmpty(boolean empty) {
        this.empty = empty;
    }
}
