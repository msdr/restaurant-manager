package com.sadur.corp.restauratmanager.model.exceptions;

public class CantGenerateDetailedViewException extends RuntimeException{


    public CantGenerateDetailedViewException(String message) {
        super(message);
    }
}
