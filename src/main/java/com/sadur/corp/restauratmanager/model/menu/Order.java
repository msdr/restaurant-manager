package com.sadur.corp.restauratmanager.model.menu;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "order_table")
public class Order implements CurrencyExchange{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@ManyToMany
	@JoinTable(name = "order_to_menu_item", joinColumns = @JoinColumn(name = "order_id"), inverseJoinColumns = @JoinColumn(name = "menu_item_id"))
	private List<MenuItem> items;
	@Column(name = "price_value")
	private BigDecimal value;
	@Enumerated(value = EnumType.STRING)
	private Currency currency;

	public Order(List<MenuItem> items, final BigDecimal value) {
		this.items = items;
		this.value = value;
		this.currency = Currency.PLN;
	}

	public Order() {
	}

	public Order(MenuItem menuItem){
		this.items = new ArrayList<>();
		items.add(menuItem);
		this.currency = menuItem.getCurrency();
		this.value = menuItem.getValue();
 	}

	@Override
	public Order exchangeCurrency(final Currency newCurrency) {
		return this;
	}

	public BigDecimal getValue() {
		return value;
	}

	public Currency getCurrency() {
		return currency;
	}

	public List<MenuItem> getItems() {
		return items;
	}
}
