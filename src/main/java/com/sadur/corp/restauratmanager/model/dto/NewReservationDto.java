package com.sadur.corp.restauratmanager.model.dto;

import com.sadur.corp.restauratmanager.model.ReservationCalendar;
import com.sadur.corp.restauratmanager.model.menu.Order;
import com.sadur.corp.restauratmanager.model.reservation.SpotType;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class NewReservationDto {

	private ReservationCalendar calendar;
	private LocalDate reservationDate;
	private LocalTime reservationTime;
	private int guestNumber;
	private List<Long> spotIndex;
	private SpotType spotType;
	private int reservedHours;
	private Order order;
	private String comment;
	
	public NewReservationDto(final ReservationCalendar calendar, LocalDate reservationDate,
	                         LocalTime reservationTime,
	                         final int guestNumber, List<Long> spotIndex, final SpotType spotType, final int reservedHours,
	                         final Order order, final String comment
	) {
		this.calendar = calendar;
		this.reservationDate = reservationDate;
		this.reservationTime = reservationTime;
		this.guestNumber = guestNumber;
		this.spotIndex = spotIndex;
		this.spotType = spotType;
		this.reservedHours = reservedHours;
		this.order = order;
		this.comment = comment;
	}

	public ReservationCalendar getCalendar() {
		return calendar;
	}

	public void setCalendar(final ReservationCalendar calendar) {
		this.calendar = calendar;
	}

	public LocalDate getReservationDate() {
		return reservationDate;
	}

	public void setReservationDate(final LocalDate reservationDate) {
		this.reservationDate = reservationDate;
	}

	public LocalTime getReservationTime() {
		return reservationTime;
	}

	public void setReservationTime(final LocalTime reservationTime) {
		this.reservationTime = reservationTime;
	}

	public int getGuestNumber() {
		return guestNumber;
	}

	public void setGuestNumber(final int guestNumber) {
		this.guestNumber = guestNumber;
	}

	public List<Long> getSpotIndex() {
		return spotIndex;
	}

	public void setSpotIndex(final List<Long> spotIndex) {
		this.spotIndex = spotIndex;
	}

	public SpotType getSpotType() {
		return spotType;
	}

	public void setSpotType(final SpotType spotType) {
		this.spotType = spotType;
	}

	public int getReservedHours() {
		return reservedHours;
	}

	public void setReservedHours(final int reservedHours) {
		this.reservedHours = reservedHours;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(final Order order) {
		this.order = order;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(final String comment) {
		this.comment = comment;
	}

	public static class Builder {
		private ReservationCalendar calendar;
		private LocalDate reservationDate;
		private LocalTime reservationTime;
		private int guestNumber;
		private List<Long> spotIndex;
		private SpotType spotType;
		private int reservedHours;
		private Order order;
		private String comment;

		public Builder withCalendar(ReservationCalendar calendar) {
			this.calendar = calendar;
			return this;
		}

		public Builder withReservationDate(LocalDate reservationDate) {
			this.reservationDate = reservationDate;
			return this;
		}

		public Builder withReservationTime(LocalTime reservationTime) {
			this.reservationTime = reservationTime;
			return this;
		}

		public Builder withGuestNumber(int guestNumber) {
			this.guestNumber = guestNumber;
			return this;
		}

		public Builder withSpotIndex(List<Long> spotIndex) {
			this.spotIndex = spotIndex;
			return this;
		}

		public Builder withSpotIndex(long spotIndex) {
			this.spotIndex = new ArrayList<>();
			this.spotIndex.add(spotIndex);
			return this;
		}

		public Builder withSpotType(SpotType spotType) {
			this.spotType = spotType;
			return this;
		}

		public Builder withReservedHours(int reservedHours) {
			this.reservedHours = reservedHours;
			return this;
		}

		public Builder withOrder(Order order) {
			this.order = order;
			return this;
		}

		public Builder withComment(String comment) {
			this.comment = comment;
			return this;
		}

		public NewReservationDto build() {
			return new NewReservationDto(this.calendar, this.reservationDate, this.reservationTime,
				this.guestNumber, this.spotIndex, this.spotType, this.reservedHours,
				this.order, this.comment
			);
		}
	}
}
