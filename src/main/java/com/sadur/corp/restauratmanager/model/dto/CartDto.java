package com.sadur.corp.restauratmanager.model.dto;

import com.sadur.corp.restauratmanager.model.cart.CartStatus;
import com.sadur.corp.restauratmanager.model.cart.Promotion;
import com.sadur.corp.restauratmanager.model.menu.MenuItem;
import com.sadur.corp.restauratmanager.model.user.User;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
@Deprecated
public class CartDto {

    private Long id;
    private BigDecimal cartValue;
    private User user;
    private List<MenuItem> menuItems = new ArrayList<>();
    private Promotion promotion;
    private CartStatus cartStatus;

    public CartDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getCartValue() {
        return cartValue;
    }

    public void setCartValue(BigDecimal cartValue) {
        this.cartValue = cartValue;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<MenuItem> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }

    public Promotion getPromotion() {
        return promotion;
    }

    public void setPromotion(Promotion promotion) {
        this.promotion = promotion;
    }

    public CartStatus getCartStatus() {
        return cartStatus;
    }

    public void setCartStatus(CartStatus cartStatus) {
        this.cartStatus = cartStatus;
    }
}