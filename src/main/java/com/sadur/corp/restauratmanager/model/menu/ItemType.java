package com.sadur.corp.restauratmanager.model.menu;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;

public enum ItemType {
    @JsonProperty("MAIN")
    MAIN,
    @JsonProperty("DESSERT")
    DESSERT,
    @JsonProperty("DRINK")
    DRINK;

    public static ItemType getItemType(String itemTypeCode) {
        return Arrays.stream(ItemType.values())
                .filter(value -> value.name().equals(itemTypeCode))
                .findFirst()
                .orElse(ItemType.MAIN);
    }

}
