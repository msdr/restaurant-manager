package com.sadur.corp.restauratmanager.model;

import com.sadur.corp.restauratmanager.model.menu.Order;
import com.sadur.corp.restauratmanager.model.reservation.ReservationSpot;
import com.sadur.corp.restauratmanager.model.user.User;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;

@Entity
@Table(name = "reservation")
public class Reservation {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@ManyToOne
	@JoinColumn(name = "spot_id")
	private ReservationSpot reservedSpot;
	//FIXME zmienna dodana na czas nauki
	//private List<ReservationSpot> reservedSpotList;

	@Column(name = "reservation_start")
	private LocalTime reservationStart;
	@Column(name = "reservation_length")
	private int reservationLength;
	@OneToOne
	@JoinColumn(name = "order_id", referencedColumnName = "id")
	private Order order;
	@Column(name = "comment")
	private String comment;
	@Column(name = "reservation_date")
	private LocalDate reservationDate;
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	//varargs
	public Reservation(final ReservationSpot reservedSpot, final LocalTime reservationStart, final int reservationLength, final LocalDate reservationDate) {
		this.reservedSpot = reservedSpot;
		this.reservationStart = reservationStart;
		this.reservationLength = reservationLength;
		this.reservationDate = reservationDate;
	}



	public Reservation() {
	}

	public User getUser() {
		return user;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public LocalDate getReservationDate() {
		return reservationDate;
	}

	public void setReservationDate(LocalDate reservationDate) {
		this.reservationDate = reservationDate;
	}

	public void setUser(User user) {
		this.user = user;
	}

	/*public List<ReservationSpot> getReservedSpotList() {
		return reservedSpotList;
	}

	public void setReservedSpotList(List<ReservationSpot> reservedSpotList) {
		this.reservedSpotList = reservedSpotList;
	}
*/
	//	public Reservation(final List<ReservationSpot> reservedSpot, final LocalTime reservationStart,
//	                   final int reservationTime, final Order order
//	) {
//		this.reservedSpot = reservedSpot;;
//		this.reservationStart = reservationStart;
//		this.reservationTime = reservationTime;
//		this.order = order;
//	}
//
//	public Reservation(final List<ReservationSpot> reservedSpot, final LocalTime reservationStart, final int reservationTime, final String comment) {
//		this.reservedSpot = reservedSpot;;
//		this.reservationStart = reservationStart;
//		this.reservationTime = reservationTime;
//		this.comment = comment;
//	}
//
//	public Reservation(final List<ReservationSpot> reservedSpot, final LocalTime reservationStart, final int reservationTime,
//	                   final Order order, final String comment) {
//		this.reservedSpot = reservedSpot;;
//		this.reservationStart = reservationStart;
//		this.reservationTime = reservationTime;
//		this.order = order;
//		this.comment = comment;
//	}
	

	public void addOrder(final Order order) {
		if (Objects.nonNull(this.order)) {
			this.order = order;
		}
	}
	
	public void addComment(final String comment) {
		if (StringUtils.isBlank(this.comment)) {
			this.comment = comment;
		}
	}

	public ReservationSpot getReservedSpot() {
		return reservedSpot;
	}

	public void setReservedSpot(final ReservationSpot reservedSpot) {
		this.reservedSpot = reservedSpot;
	}

	public LocalTime getReservationStart() {
		return reservationStart;
	}
	public void setReservationStart(LocalTime reservationStart) {
		this.reservationStart = reservationStart;
	}
	public int getReservationLength() {
		return reservationLength;
	}

	public void setReservationLength(final int reservationLength) {
		this.reservationLength = reservationLength;
	}
	
	public Order getOrder() {
		return order;
	}

	public String getComment() {
		return comment;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		final Reservation that = (Reservation) o;
		return reservationLength == that.reservationLength && Objects.equals(reservedSpot, that.reservedSpot) && Objects.equals(reservationStart, that.reservationStart);
	}

	@Override
	public int hashCode() {
		return Objects.hash(reservedSpot, reservationStart, reservationLength);
	}

	@Override
	public String toString() {
		return "Reservation{" +
				"id=" + id +
				", reservedSpot=" + reservedSpot +
				", reservationStart=" + reservationStart +
				", reservationTime=" + reservationLength +
				", order=" + order +
				", comment='" + comment + '\'' +
				", reservationDate=" + reservationDate +
				'}';
	}
}
