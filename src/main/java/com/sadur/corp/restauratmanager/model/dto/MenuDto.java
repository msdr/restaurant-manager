package com.sadur.corp.restauratmanager.model.dto;

import com.sadur.corp.restauratmanager.model.menu.Menu;

import java.time.format.DateTimeFormatter;

public class MenuDto {

    private Long id;
    private String name;
    private String availableFrom;
    private String availableTill;
    private boolean available;

    public MenuDto() {
    }

    public MenuDto(Menu menu){
        this.id = menu.getId();
        this.name = menu.getName();
        this.availableFrom = menu.getAvailableFrom().format(DateTimeFormatter.ISO_DATE);
        this.availableTill = menu.getAvailableTill().format(DateTimeFormatter.ISO_DATE);
        this.available = menu.isAvailable();
    }

    public MenuDto(Long id, String name, String availableFrom, String availableTill, boolean available) {
        this.id = id;
        this.name = name;
        this.availableFrom = availableFrom;
        this.availableTill = availableTill;
        this.available = available;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvailableFrom() {
        return availableFrom;
    }

    public void setAvailableFrom(String availableFrom) {
        this.availableFrom = availableFrom;
    }

    public String getAvailableTill() {
        return availableTill;
    }

    public void setAvailableTill(String availableTill) {
        this.availableTill = availableTill;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }
}
