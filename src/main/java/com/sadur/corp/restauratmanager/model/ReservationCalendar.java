package com.sadur.corp.restauratmanager.model;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

public class ReservationCalendar {
	private Set<ParticularDay> daysList;

	public ReservationCalendar() {
		this.daysList = new HashSet<>();
	}

	public void addNewDay(ParticularDay dayToAdd) {
		if (!isInCalendar(dayToAdd.getDate())) {
			this.daysList.add(dayToAdd);
		}
	}

	public boolean isInCalendar(final LocalDate day) {
		return this.daysList.stream()
			.anyMatch(particularDay -> particularDay.getDate().equals(particularDay)); // boolean
	}

	public boolean isInCalendar(final ParticularDay day) {
		return this.daysList.stream()
			.anyMatch(particularDay -> particularDay.getDate().equals(particularDay.getDate())); // boolean
	}

	public Set<ParticularDay> getDaysList() {
		return daysList;
	}

	@Override
	public String toString() {
		return "ReservationCalendar{" +
			"daysList=" + daysList +
			'}';
	}
}
