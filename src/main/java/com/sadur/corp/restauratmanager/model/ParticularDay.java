package com.sadur.corp.restauratmanager.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ParticularDay {
	private LocalDate date;
	private List<Reservation> reservationsList;

	public ParticularDay(final LocalDate date) {
		this.date = date;
		this.reservationsList = new ArrayList<>();
	}

	public LocalDate getDate() {
		return date;
	}

	public List<Reservation> getReservationsList() {
		return reservationsList;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		final ParticularDay that = (ParticularDay) o;
		return Objects.equals(date, that.date) && Objects.equals(reservationsList, that.reservationsList);
	}

	@Override
	public int hashCode() {
		return Objects.hash(date, reservationsList);
	}

	@Override
	public String toString() {
		return "ParticularDay{" +
			"date=" + date +
			", reservationsList=" + reservationsList +
			'}';
	}
}
