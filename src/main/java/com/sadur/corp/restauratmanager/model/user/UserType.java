package com.sadur.corp.restauratmanager.model.user;

public enum UserType {
	ADMIN,
	USER;
}
