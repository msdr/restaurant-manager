package com.sadur.corp.restauratmanager.model.user;

import com.sadur.corp.restauratmanager.model.cart.Cart;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "users")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "name")
	private String name;
	@Column(name = "email")
	private String email;
	@Column(name = "password")
	private String password;
	@Column(name = "role")
	@Enumerated(value = EnumType.STRING)
	private UserType role;
	@OneToMany(mappedBy = "user")
	private Set<Cart> carts;


	public User(Long id, String name, String email, String password, UserType role) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.password = password;
		this.role = role;
	}

	public User(String email, String password) {
		this.email = email;
		this.password = password;
		this.role = UserType.ADMIN;
	}


	public User() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public UserType getRole() {
		return role;
	}

	public void setRole(UserType role) {
		this.role = role;
	}
}