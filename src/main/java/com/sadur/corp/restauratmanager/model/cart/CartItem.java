package com.sadur.corp.restauratmanager.model.cart;

import com.sadur.corp.restauratmanager.model.AbstractEntity;
import com.sadur.corp.restauratmanager.model.menu.MenuItem;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "cart_items")
public class CartItem extends AbstractEntity {

	@ManyToOne
	@JoinColumn(name = "cart_id")
	private Cart cart;
	@ManyToOne
	@JoinColumn(name = "menu_item_id")
	private MenuItem menuItem;
	@Column(name = "price")
	private BigDecimal price;

	public CartItem() {
	}

	public CartItem(final MenuItem menuItem, final BigDecimal price) {
		this.menuItem = menuItem;
		this.price = price;
	}
	
	public void setCart(final Cart cart) {
		this.cart = cart;
	}

	public Cart getCart() {
		return cart;
	}

	public MenuItem getMenuItem() {
		return menuItem;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}
}
