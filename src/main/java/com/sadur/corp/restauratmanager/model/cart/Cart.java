package com.sadur.corp.restauratmanager.model.cart;

import com.sadur.corp.restauratmanager.model.AbstractEntity;
import com.sadur.corp.restauratmanager.model.menu.Currency;
import com.sadur.corp.restauratmanager.model.user.User;


import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "cart")
public class Cart extends AbstractEntity {

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "cart_id", referencedColumnName = "id")
    private List<CartItem> menuItems = new ArrayList<>();
    @Column(name = "cart_value")
    private BigDecimal cartValue;
    @Enumerated(EnumType.STRING)
    @Column(name = "promotion")
    private Promotion promotion;
    @Enumerated(EnumType.STRING)
    @Column(name = "currency")
    private Currency currency;
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private CartStatus cartStatus;

    public Cart() {
        this.cartValue = BigDecimal.ZERO;
        this.cartStatus = CartStatus.ACTIVE;
    }

    public Cart(User user) {
        this.user = user;
        this.cartStatus = CartStatus.ACTIVE;
        this.cartValue = BigDecimal.ZERO;

    }


    public void addToCart(CartItem menuItem){
        menuItems.add(menuItem);
    }

    public void removeFromCart(CartItem menuItem){
        menuItems.remove(menuItem);
    }

    public void confirmCart(){
        cartStatus = CartStatus.CONFIRMED;
    }

    public User getUser() {
        return user;
    }

    public List<CartItem> getMenuItems() {
        return menuItems;
    }

    public BigDecimal getCartValue() {
        return cartValue;
    }

    public void setCartValue(BigDecimal cartValue) {
        this.cartValue = cartValue;
    }

    public Promotion getPromotion() {
        return promotion;
    }

    public Currency getCurrency() {
        return currency;
    }

    public CartStatus getCartStatus() {
        return cartStatus;
    }
}
