package com.sadur.corp.restauratmanager.model.menu;

public interface CurrencyExchange {
	
	<T> T exchangeCurrency(final Currency newCurrency);
}
