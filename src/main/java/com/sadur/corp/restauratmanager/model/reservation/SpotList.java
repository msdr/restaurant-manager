package com.sadur.corp.restauratmanager.model.reservation;

import java.util.List;
import java.util.ArrayList;

public class SpotList {
    private List<ReservationSpot> reservationSpots;

    public SpotList(){
        this.reservationSpots = new ArrayList<>();
    }
    public SpotList(List<ReservationSpot> reservationSpots) {
        this.reservationSpots = reservationSpots;
    }

    public List<ReservationSpot> getReservationSpots() {
        return reservationSpots;
    }

    @Override
    public String toString() {
        return "SpotList{" +
                "reservationSpots=" + reservationSpots +
                '}';
    }
}
