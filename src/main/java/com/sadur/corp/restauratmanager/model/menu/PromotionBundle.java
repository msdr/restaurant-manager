package com.sadur.corp.restauratmanager.model.menu;

import javax.persistence.Embeddable;

@Embeddable
public class PromotionBundle {

    private boolean standard;
    private boolean extra;
    private boolean premium;

    public PromotionBundle() {
    }

    public PromotionBundle(boolean standard, boolean extra, boolean premium) {
        this.standard = standard;
        this.extra = extra;
        this.premium = premium;
    }

    public boolean isStandard() {
        return standard;
    }

    public void setStandard(boolean standard) {
        this.standard = standard;
    }

    public boolean isExtra() {
        return extra;
    }

    public void setExtra(boolean extra) {
        this.extra = extra;
    }

    public boolean isPremium() {
        return premium;
    }

    public void setPremium(boolean premium) {
        this.premium = premium;
    }
}
