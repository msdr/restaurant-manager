package com.sadur.corp.restauratmanager.model.exceptions;

public class CantDeleteReservationException extends Exception {

	public CantDeleteReservationException(final String message) {
		super(message);
	}
}
