package com.sadur.corp.restauratmanager.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

//@JsonIgnoreProperties(ignoreUnknown = true)
@Deprecated
public class MenuItemDto {
    @JsonProperty("name")
    private String name;
    @JsonProperty("value")
    private BigDecimal value;
    @JsonProperty("currency")
    private String currency;

    public MenuItemDto(String name, BigDecimal value, String currency) {
        this.name = name;
        this.value = value;
        this.currency = currency;
    }

    public MenuItemDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return "MenuItemDto{" +
                "name='" + name + '\'' +
                ", value=" + value +
                ", currency=" + currency +
                '}';
    }
}
