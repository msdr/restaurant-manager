package com.sadur.corp.restauratmanager.model.cart;

public enum CartStatus {

    ACTIVE,
    CONFIRMED,
    ABANDONED

}
