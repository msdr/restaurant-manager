package com.sadur.corp.restauratmanager.model.reservation;

public enum SpotType {
	TABLE,
	BAR,
    LOUNGE;
}
