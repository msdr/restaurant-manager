package com.sadur.corp.restauratmanager.model.reservation;

import javax.persistence.Table;
import javax.persistence.*;


@Entity
@Table(name = "spots")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "spot_type", discriminatorType = DiscriminatorType.STRING)
public abstract class ReservationSpot {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "seats_number")
    private int seatsNumber;
    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private SpotType spotType;
//    @ManyToMany
//    @JoinTable(name = "spots_to_spots",
//            joinColumns = @JoinColumn(name = "spot_id"),
//            inverseJoinColumns = @JoinColumn(name = "neighbour_id"))
//    List<ReservationSpot> neighbourSpots;

    public ReservationSpot(int seatsNumber, SpotType spotType) {
        this.seatsNumber = seatsNumber;
        this.spotType = spotType;
    }

    public ReservationSpot() {
    }

    public int getSeatsNumber() {
        return seatsNumber;
    }
    //created for test
    public void setSeatsNumber(int seatsNumber) {
        this.seatsNumber = seatsNumber;
    }

    public long getTableIndex() {
        return id;
    }

    public SpotType getSpotType() {
        return spotType;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "ReservationSpot{" +
                "tableIndex=" + id +
                ", seatsNumber=" + seatsNumber +
                ", spotType=" + spotType +
                '}';
    }
}
