package com.sadur.corp.restauratmanager.model.menu;

import com.sadur.corp.restauratmanager.model.AbstractEntity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "menu_items")
public class MenuItem extends AbstractEntity implements CurrencyExchange {

    @Column(name = "name")
    private String name;
    @Column(name = "value")
    private BigDecimal value;
    @Enumerated(EnumType.STRING)
    private Currency currency;
    @ManyToMany
    @JoinTable(name = "order_to_menu_item", joinColumns = @JoinColumn(name = "menu_item_id"), inverseJoinColumns = @JoinColumn(name = "order_id"))
    private List<Order> orders;
    @ManyToMany
    @JoinTable(name = "menus_to_menu_items", joinColumns = @JoinColumn(name = "menu_item_id"), inverseJoinColumns = @JoinColumn(name = "menu_id"))
    private List<Menu> menus;
    @Column(name = "item_type")
    private ItemType itemType;
    @Embedded
    @Column(name = "promotion_bundle")
    private PromotionBundle promotionBundle;

    public MenuItem(final String name, final BigDecimal value, final Currency currency) {
        this.name = name;
        this.value = value;
        this.currency = Currency.PLN;
    }

    public MenuItem(BigDecimal value, ItemType itemType, PromotionBundle promotionBundle) {
        this.value = value;
        this.itemType = itemType;
        this.promotionBundle = promotionBundle;
    }

    public MenuItem(String name, BigDecimal value, Currency currency, ItemType itemType, PromotionBundle promotionBundle) {
        this.name = name;
        this.value = value;
        this.currency = currency;
        this.itemType = itemType;
        this.promotionBundle = promotionBundle;
    }

    public MenuItem() {
    }

    @Override
    public MenuItem exchangeCurrency(final Currency newCurrency) {
        return this;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getValue() {
        return value;
    }

    public Currency getCurrency() {
        return currency;
    }

    @Override
    public String toString() {
        return "MenuItem{" +
                "name='" + name + '\'' +
                ", value=" + value +
                ", currency=" + currency +
                '}';
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public List<Menu> getMenus() {
        return menus;
    }

    public void setMenus(List<Menu> menus) {
        this.menus = menus;
    }

    public ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public PromotionBundle getPromotionBundle() {
        return promotionBundle;
    }

    public void setPromotionBundle(PromotionBundle promotionBundle) {
        this.promotionBundle = promotionBundle;
    }
}
