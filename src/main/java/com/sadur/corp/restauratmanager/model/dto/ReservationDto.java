package com.sadur.corp.restauratmanager.model.dto;

import com.sadur.corp.restauratmanager.model.reservation.SpotType;

public class ReservationDto {

    private String reservationDate;
    private String reservationStart;
    private int guestNumber;
    private int reservationLength;
    private SpotType spotType;

    public ReservationDto(String reservationDate, String reservationStart,
                          int guestNumber,
                          int reservationLength, SpotType spotType) {
        this.reservationDate = reservationDate;
        this.reservationStart = reservationStart;
        this.guestNumber = guestNumber;
        this.reservationLength = reservationLength;
        this.spotType = spotType;
    }

    public ReservationDto() {
    }

    public String getReservationDate() {
        return reservationDate;
    }

    public void setReservationDate(String reservationDate) {
        this.reservationDate = reservationDate;
    }

    public String getReservationStart() {
        return reservationStart;
    }

    public void setReservationStart(String reservationStart) {
        this.reservationStart = reservationStart;
    }

    public int getGuestNumber() {
        return guestNumber;
    }

    public void setGuestNumber(int guestNumber) {
        this.guestNumber = guestNumber;
    }

    public int getReservationLength() {
        return reservationLength;
    }

    public void setReservationLength(int reservationLength) {
        this.reservationLength = reservationLength;
    }

    public SpotType getSpotType() {
        return spotType;
    }

    public void setSpotType(SpotType spotType) {
        this.spotType = spotType;
    }

    @Override
    public String toString() {
        return "ReservationDto{" +
                "reservationDate=" + reservationDate +
                ", reservationTime=" + reservationStart +
                ", guestNumber=" + guestNumber +
                ", reservedHours=" + reservationLength +
                ", spotType='" + spotType + '\'' +
                '}';
    }
}
