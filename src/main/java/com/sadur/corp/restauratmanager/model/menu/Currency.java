package com.sadur.corp.restauratmanager.model.menu;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;

public enum Currency {
	@JsonProperty("PLN")
	PLN,
	@JsonProperty("DOL")
	DOL,
	@JsonProperty("EUR")
	EUR;

	public static Currency getCurrency(String currencyCode) {
		return Arrays.stream(Currency.values())
			.filter(value -> value.name().equals(currencyCode))
			.findFirst()
			.orElse(Currency.PLN);
	}
}
