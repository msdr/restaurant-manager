package com.sadur.corp.restauratmanager.model.exceptions;

public class CantUpdateReservationException extends RuntimeException{
    public CantUpdateReservationException(String message) {
        super(message);
    }
}
