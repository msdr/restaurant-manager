package com.sadur.corp.restauratmanager.model.cart;

public enum Promotion {

    HAPPY_HOUR(0.5f, 4, 0),
    DISCOUNT_10(0.1f, 1, 0),
    DISCOUNT_20(0.2f, 1, 0),
    TWO_FOR_ONE(0.5f, 2, 0),
    DISCOUNT_SET(0.1f, 3,0),
    DISCOUNT(1f, 1, 50),
    TOTAL_CART_PERCENT_DISCOUNT(0.33f, 3, 0);


    private float discountPercent;
    private int minimalItemsNumber;
    private int discountValue;


    Promotion(float discountPercent, int minimalItemsNumber, int discountValue) {
        this.discountPercent = discountPercent;
        this.minimalItemsNumber = minimalItemsNumber;
        this.discountValue = discountValue;

    }

    public int getDiscountValue() {
        return discountValue;
    }

    public int getMinimalItemsNumber() {
        return minimalItemsNumber;
    }

    public float getDiscountPercent() {
        return discountPercent;
    }
}
