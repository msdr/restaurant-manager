package com.sadur.corp.restauratmanager.model.menu;


import com.sadur.corp.restauratmanager.model.AbstractEntity;
import com.sadur.corp.restauratmanager.model.dto.MenuDto;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "menus")
public class Menu extends AbstractEntity {

	@Column(name = "name")
	private String name;
	@ManyToMany
	@JoinTable(name = "menus_to_menu_items", joinColumns = @JoinColumn(name = "menu_id"), inverseJoinColumns = @JoinColumn(name = "menu_item_id"))
	private List<MenuItem> menuItems;
	@Column(name = "available_from")
	private LocalDate availableFrom;
	@Column(name = "available_till")
	private LocalDate availableTill;
	@Column(name = "available")
	private boolean available;
	@Column(name = "active")
	private boolean active;

	public Menu() {
	}

	public Menu(MenuDto menuDto){
		this.name = menuDto.getName();
		this.availableFrom =(StringUtils.isNotBlank(menuDto.getAvailableFrom())) ? LocalDate.parse(menuDto.getAvailableFrom()) : null;
		this.availableTill =(StringUtils.isNotBlank(menuDto.getAvailableTill())) ? LocalDate.parse(menuDto.getAvailableTill()) : null;
		this.available = menuDto.isAvailable();
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<MenuItem> getMenuItems() {
		return menuItems;
	}

	public Menu(List<MenuItem> menuItems){
		this.menuItems = menuItems;
	}

	public void setMenuItems(List<MenuItem> menuItems) {
		this.menuItems = menuItems;
	}

	public LocalDate getAvailableFrom() {
		return availableFrom;
	}

	public void setAvailableFrom(LocalDate availableFrom) {
		this.availableFrom = availableFrom;
	}

	public LocalDate getAvailableTill() {
		return availableTill;
	}

	public void setAvailableTill(LocalDate availableTill) {
		this.availableTill = availableTill;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
}