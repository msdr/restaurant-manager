package com.sadur.corp.restauratmanager.model.exceptions;

public class CantCreateReservationException extends RuntimeException{
    public CantCreateReservationException(String message) {
        super(message);
    }
}
