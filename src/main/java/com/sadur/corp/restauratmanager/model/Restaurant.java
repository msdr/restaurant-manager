package com.sadur.corp.restauratmanager.model;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.List;

import com.sadur.corp.restauratmanager.model.reservation.ReservationSpot;

public class Restaurant {
	private LocalTime openingHour;
	private LocalTime closingHour;
	private List<DayOfWeek> openingDays;
	private List<ReservationSpot> reservationSpots; 
	
	private Restaurant() {
	}
	
	private static Restaurant INSTANCE = null;


	public static Restaurant get() {
		if (INSTANCE == null) {
			INSTANCE = new Restaurant();
		}

		return INSTANCE;
	}


	public LocalTime getOpeningHour() {
		return openingHour;
	}

	public LocalTime getClosingHour() {
		return closingHour;
	}

	public List<DayOfWeek> getOpeningDays() {
		return openingDays;
	}

	public List<ReservationSpot> getReservationSpots() {
		return reservationSpots;
	}

	public void setOpeningHour(final LocalTime openingHour) {
		this.openingHour = openingHour;
	}

	public void setClosingHour(final LocalTime closingHour) {
		this.closingHour = closingHour;
	}

	public void setOpeningDays(final List<DayOfWeek> openingDays) {
		this.openingDays = openingDays;
	}

	public void setReservationSpots(final List<ReservationSpot> reservationSpots) {
		this.reservationSpots = reservationSpots;
	}

	@Override
	public String toString() {
		return "Restaurant{" +
			"openingHour=" + openingHour +
			", closingHour=" + closingHour +
			", openingDays=" + openingDays +
			", reservationSpots=" + reservationSpots +
			'}';
	}
}
