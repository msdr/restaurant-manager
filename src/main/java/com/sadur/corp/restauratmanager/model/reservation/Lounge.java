package com.sadur.corp.restauratmanager.model.reservation;

import javax.persistence.Entity;

@Entity
public class Lounge extends ReservationSpot {

    public Lounge(int seatsNumber) {
        super(seatsNumber, SpotType.LOUNGE);
    }

    public Lounge() {
    }

    public String toString() {
		return super.toString();
	}
}
