package com.sadur.corp.restauratmanager.model.exceptions;

public class ReservationNotFoundException extends Exception {
	public ReservationNotFoundException(final String message) {
		super(message);
	}
}
