package com.sadur.corp.restauratmanager.model.dto;

public class SimpleMenuDto {

    private Long id;
    private String name;
    private boolean available;

    public SimpleMenuDto(Long id, String name, boolean available) {
        this.id = id;
        this.name = name;
        this.available = available;
    }

    public SimpleMenuDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }
}
